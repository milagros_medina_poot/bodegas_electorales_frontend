import React, { useEffect, useState } from "react"
import { Container } from "react-bootstrap";
import {Row, Card, Button} from "react-bootstrap";
import {Col, Form} from "react-bootstrap";
import { CSVLink} from "react-csv";
import { Modal } from "react-bootstrap";
import {config} from "../Constants"

const FuncionamientoCrytDat = () => {
     const url = config.url.BASE_URL;

    let usuario = {}
    if(localStorage.getItem("usuario")){
        usuario = JSON.parse(localStorage.getItem("usuario"))
    }
    
    useEffect(() => {
        if(usuario.rol === "Admin"){
            console.log(usuario.rol)
            getData()
        } else if(usuario.rol === "usuario"){
            console.log(usuario.rol)
            getDataByIdUsuario()
        }
        
    }, [])

    

    const [mensaje, setMensaje] = useState("")
    const [show, setShow] = useState(false);
    const handleClose = () => {
        setShow(false);
        window.location.reload()
    }
    const handleShow = () => setShow(true);

    //Codigo para la generacion del archivo csv
    const [dataList, setDataList] = useState([])
    const headers = [
        { label: "ID_ESTADO", key: "id_estado" },
        { label: "NOMBRE_ESTADO", key: "nombre_estado" },
        { label: "ID_DISTRITO_FED", key: "id_distrito_federal" },
        { label: "CABECERA_DISTRITAL_FED", key: "cabecera_distrital_federal" },
        { label: "ID_DISTRITO_LOC", key: "id_distrito_local" },
        { label: "CABECERA_DISTRITAL_LOC", key: "cabecera_distrital_local" },
        { label: "ID_MUNICIPIO", key: "id_municipio_local" },
        { label: "MUNICIPIO", key: "municipio_local" },
        { label: "ID_CRYT_DAT", key: "id_cryt_dat" },
        { label: "TIPO_CRYT_DAT", key: "tipo_cryt_dat" },
        { label: "FECHA_HORA_APERTURA", key: "fecha_hora_apertura" },
        { label: "FECHA_HORA_CIERRE", key: "fecha_hora_cierre" },
        { label: "HORAS_TRASLADO", key: "horas_traslado" },
        { label: "MINUTOS_TRASLADO", key: "minutos_traslado" },
        { label: "DISTANCIA_TRASLADO_DESTINO_FINAL", key: "distancia_traslado_destino_final" },
        { label: "FECHA_HORA_INICIO_CUSTODIA_FUN", key: "fecha_hora_inicio_custodia_fun" },
        { label: "FECHA_HORA_FIN_CUSTODIA_FUN", key: "fecha_hora_fin_custodia_fun" },
        { label: "FECHA_HORA_INICIO_CUSTODIA_TRA", key: "fecha_hora_incio_custodia_tra" },
        { label: "FECHA_HORA_FIN_CUSTODIA_TRA", key: "fecha_hora_fin_custodia_tra" },
        { label: "INCIDENTES_CRYT_DAT", key: "incidentes_cryt_dat" },
        { label: "SOLUCIONES_CRYT_DAT", key: "soluciones_cryt_dat" }
      ];

    //Metodo para obtener datos de la tabla y generar archivo csv
    const getDataByIdUsuario = () => {
        const data = {id_usuario:usuario.id}
        fetch(url + "/funcionamiento_cryt_dat_byidusuario", {
                method: "POST",
                headers:{
                    "Content-Type": "application/json"
                },
                body: JSON.stringify(data)
            })
            .then(response => response.json())
            .then(result => {
                setDataList(result)
                console.log(result)
                
            })
            .catch(error => {
                console.log(error)
            })
    }
    
    
    //Metodo get simple
    const getData = () => {
        fetch(url + "/funcionamiento_cryt_dat", {
            method: "GET",
            headers:{
                "Content-Type": "application/json"
            }
        })
            .then(response => response.json())
            .then(result => {

                setDataList(result)
                console.log(result)
                
            })
            .catch(error => {
                console.log(error)
            })
    }

    //variables del documento
    const [id_cryt_dat, setIdCrytDat] = useState()
    const [tipo_cryt_dat, setTipoCrytDat] = useState("F")
    const [fecha_hora_apertura, setFechaHoraApertura] = useState()
    const [fecha_hora_cierre, setFechaHoraCierre] = useState()
    const [horas_traslado, setHorasTraslado] = useState()
    const [minutos_traslado, setMinutosTraslado] = useState()
    const [distancia_traslado_destino_final, setDistanciaTrasladoDestinoFinal] = useState()
    const [fecha_hora_inicio_custodia_fun, setFechaHoraInicioCustodiaFun] = useState()
    const [fecha_hora_fin_custodia_fun, setFechaHoraFinCustodiaFun] = useState()
    const [fecha_hora_inicio_custodia_tra, setFechaHoraInicioCustodiaTra] = useState()
    const [fecha_hora_fin_custodia_tra, setFechaHoraFinCustodiaTra] = useState()
    const [incidentes_cryt_dat, setIncidentesCrytDat] = useState("NINGUNO")
    const [soluciones_cryt_dat, setSolucionesCrytDat] = useState("NO APLICA")

    //Validacion de strings
    const [errorIdCrytDat, setErrorIdCrytDat] = useState(false)
    const [errorIncidentes, setErrorIncidentes] = useState(false)
    const [errorSoluciones, setErrorSoluciones] = useState(false)
    const regex = RegExp(/^[A-Za-z0-9 ]+$/)
    const regexIncidentesSoluciones = RegExp(/^[A-Za-z ]+$/)

    const handleIdCrytDat = (e) => {
        setIdCrytDat(e.target.value)

        if(e.target.value != ""){
            if(!regex.test(e.target.value)){
                setErrorIdCrytDat(true)
            }else{
                setErrorIdCrytDat(false)
            }
        }else{
            setErrorIdCrytDat(false)
        }  
    }

    const handleIncidentes = (e) => {
        setIncidentesCrytDat(e.target.value)

        if(e.target.value != ""){
            if(!regexIncidentesSoluciones.test(e.target.value)){
                setErrorIncidentes(true)
            }else{
                setErrorIncidentes(false)
            }
        }else{
            setErrorIncidentes(false)
            setIncidentesCrytDat("NINGUNO")
        }  
    }

    const handleSoluciones = (e) => {
        setSolucionesCrytDat(e.target.value)

        if(e.target.value != ""){
            if(!regexIncidentesSoluciones.test(e.target.value)){
                setErrorSoluciones(true)
            }else{
                setErrorSoluciones(false)
            }
        }else{
            setErrorSoluciones(false)
            setSolucionesCrytDat("NO APLICA")
        }  
    }

    //Metodo para nuevo registro de FUNCIONAMIENTO DE CRYT Y DAT
    const nuevoRegistro = (e) => {
        e.preventDefault()
        const data = {
            id_usuario: usuario.id,
            id_cryt_dat: id_cryt_dat.toLocaleUpperCase(),
            tipo_cryt_dat: tipo_cryt_dat,
            fecha_hora_apertura: fecha_hora_apertura,
            fecha_hora_cierre: fecha_hora_cierre,
            horas_traslado: horas_traslado,
            minutos_traslado: minutos_traslado,
            distancia_traslado_destino_final: distancia_traslado_destino_final,
            fecha_hora_inicio_custodia_fun: fecha_hora_inicio_custodia_fun,
            fecha_hora_fin_custodia_fun: fecha_hora_fin_custodia_fun,
            fecha_hora_inicio_custodia_tra: fecha_hora_inicio_custodia_tra,
            fecha_hora_fin_custodia_tra: fecha_hora_fin_custodia_tra,
            incidentes_cryt_dat: incidentes_cryt_dat.toLocaleUpperCase(),
            soluciones_cryt_dat: soluciones_cryt_dat.toLocaleUpperCase()
        }

        if(!errorIdCrytDat && !errorIncidentes && !errorSoluciones){
            fetch(url + "/registrar_funcionamiento_cryt_dat", {
                method: "POST",
                headers:{
                    "Content-Type": "application/json"
                },
                body: JSON.stringify(data)
            })
            .then(response => response.json())
            .then(result => {
                setMensaje("Registro guardado con éxito")
                handleShow()
                console.log(result)
                
            })
            .catch(error => {
                setMensaje(error)
                handleShow()
                console.log(error)
            })
        }
    }

    const [loading, setLoading] = useState(false);

    

    return(
        <>
        <Container>
            <h5>Funcionamiento de CRYT y DAT</h5>
            
            <Row>
                <Col sm={10}>
                <p>Ingrese los siguientes datos para el registro de la información general de la operación del mecanismo de recolección, en caso de ser operado por personal contratado por el OPLE</p>
                </Col>
                <Col sm={2}>
                <CSVLink data={dataList} headers={headers} filename="FUNCIONAMIENTO DE CRYT Y DAT">
                        <Button className="button-custom">
                            {loading ? 'Generando...' : 'Descargar'}
                        </Button>
                    </CSVLink>
                </Col>
            </Row>


            <Form onSubmit={nuevoRegistro}>
            <Row className="mb-3">
                <Col xs lg="6">
                    <Card className="mt-1">
                        <Card.Body>
                            <Card.Text>
                                <Form.Group className="mb-4">
                                    <Form.Label>Identificador de CRYT/DAT</Form.Label>
                                    <Form.Control required onChange={(e)=>handleIdCrytDat(e)} style={{ textTransform: 'uppercase'}}/>
                                    <Form.Text className="text-muted" id="error">
                                    Solo se permiten letras y números. Sin acentos ni caracteres especiales.
                                    </Form.Text>
                                    {errorIdCrytDat && <p style={{color: "red"}}>El texto es inválido</p>}
                                </Form.Group>
                                <Form.Group className="mb-4">
                                    <Form.Label>Tipo de CRYT/DAT</Form.Label>
                                    <Form.Select onChange={(e)=>setTipoCrytDat(e.target.value)}>
                                        <option value="F">Fijo</option>
                                        <option value="I">Itinerante</option>
                                        <option value="D">DAT</option>
                                    </Form.Select>
                                </Form.Group>
                                <Form.Group className="mb-4">
                                    <Form.Label>Registra la fecha y hora de apertura del CRYT o de inicio de operaciones del DAT</Form.Label>
                                    <Form.Control type="datetime-local" required onChange={(e)=>setFechaHoraApertura(e.target.value)}/>
                                </Form.Group>
                                <Form.Group className="mb-4">
                                    <Form.Label>Registra la fecha y hora del cierre del CRYT o de termino de operaciones del DAT</Form.Label>
                                    <Form.Control type="datetime-local" required onChange={(e)=>setFechaHoraCierre(e.target.value)}/>
                                </Form.Group>
                                <Form.Group className="mb-4">
                                    <Form.Label>Horas de traslado al lugar de entrega</Form.Label>
                                    <Form.Control type="number" min={0} required onChange={(e)=>setHorasTraslado(e.target.value)}/>
                                </Form.Group>
                                <Form.Group className="mb-4">
                                    <Form.Label>Minutos de traslado al lugar de entrega</Form.Label>
                                    <Form.Control type="number" min={0} max={59} required onChange={(e)=>setMinutosTraslado(e.target.value)}/>
                                </Form.Group>
                                <Form.Group className="mb-4">
                                    <Form.Label>Distancia de traslado del CRYT al destino final, en kilómetros</Form.Label>
                                    <Form.Control type="number" min={1} required onChange={(e)=>setDistanciaTrasladoDestinoFinal(e.target.value)}/>
                                </Form.Group>
                                <Form.Group className="mb-4">
                                    <Form.Label>Registra la fecha y hora del inicio de la custodia durante el funcionamiento del CRYT</Form.Label>
                                    <Form.Control type="datetime-local" required onChange={(e)=>setFechaHoraInicioCustodiaFun(e.target.value)}/>
                                </Form.Group>
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Col>

                <Col xs lg="6">
                    <Card className="mt-1">
                        <Card.Body>
                            <Card.Text>
                                <Form.Group className="mb-4">
                                    <Form.Label>Registra la fecha y hora del fin de la custodia durante el funcionamiento del CRYT</Form.Label>
                                    <Form.Control type="datetime-local" required onChange={(e)=>setFechaHoraFinCustodiaFun(e.target.value)}/>
                                </Form.Group>
                                <Form.Group className="mb-4">
                                    <Form.Label>Registra la fecha y hora del inicio de la custodia durante el traslado del CRYT al consejo distrital</Form.Label>
                                    <Form.Control type="datetime-local" required onChange={(e)=>setFechaHoraInicioCustodiaTra(e.target.value)}/>
                                </Form.Group>
                                <Form.Group className="mb-4">
                                    <Form.Label>Registra la fecha y hora del fin de la custodia durante el traslado del CRYT al consejo distrital</Form.Label>
                                    <Form.Control type="datetime-local" required onChange={(e)=>setFechaHoraFinCustodiaTra(e.target.value)}/>
                                </Form.Group>
                                <Form.Group className="mb-4">
                                    <Form.Label>En caso de incidentes presentados en la operacion del CRYT fijo, itinerante o DAT, realice una breve descripción</Form.Label>
                                    <Form.Control as="textarea" onChange={(e)=>handleIncidentes(e)} style={{ textTransform: 'uppercase'}}/>
                                    <Form.Text className="text-muted" id="error">
                                        Solo se permiten letras. Sin acentos ni caracteres numéricos o especiales.
                                    </Form.Text>
                                    {errorIncidentes && <p style={{color: "red"}}>El texto es inválido</p>}
                                </Form.Group>
                                <Form.Group className="mb-4">
                                    <Form.Label>Soluciones implementadas para los incidentes presentados en el CRYT fijo, itinerante o DAT</Form.Label>
                                    <Form.Control as="textarea" onChange={(e)=>handleSoluciones(e)} style={{ textTransform: 'uppercase'}}/>
                                    <Form.Text className="text-muted" id="error">
                                        Solo se permiten letras. Sin acentos ni caracteres numéricos o especiales.
                                    </Form.Text>
                                    {errorSoluciones && <p style={{color: "red"}}>El texto es inválido</p>}
                                </Form.Group>
                            </Card.Text>
                        </Card.Body>
                    </Card>
                    <div className="d-grid gap-2 mt-3">
                        <Button type="submit" className="button-custom">Guardar</Button>
                    </div>
                </Col>
            </Row>
            </Form>
        </Container>

        <Modal
            show={show}
            onHide={handleClose}
            backdrop="static"
            keyboard={false}
        >
            <Modal.Header closeButton>
                <Modal.Title></Modal.Title>
            </Modal.Header>
            <Modal.Body>
            {mensaje}
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={handleClose}>
                    Cerrar
                </Button>
            </Modal.Footer>
        </Modal>
        </>
    )

}

export default FuncionamientoCrytDat