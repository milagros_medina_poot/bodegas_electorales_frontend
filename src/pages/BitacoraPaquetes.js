import React, { useEffect, useState } from "react"
import { Container } from "react-bootstrap";
import {Row, Card, Button} from "react-bootstrap";
import {Col, Form} from "react-bootstrap";
import { CSVLink } from "react-csv";
import { Modal } from "react-bootstrap";
import Select from 'react-select';
import {config} from "../Constants"

const BitacoraPaquetes = () => {
    const url = config.url.BASE_URL;

    //Obtenemos usuario desde el LocalStorage
    let usuario = {}
    if(localStorage.getItem("usuario")){
        usuario = JSON.parse(localStorage.getItem("usuario"))
    }
    
    //Validamos el rol del usuario
    useEffect(() => {
        if(usuario.rol === "Admin"){
            console.log(usuario.rol)
            getData()
            getCasillas()
        } else if(usuario.rol === "usuario"){
            console.log(usuario.rol)
            getDataByIdUsuario()
            getCasillas()
        }
      }, [])


    const [mensaje, setMensaje] = useState("")
    const [show, setShow] = useState(false);
    const handleClose = () => {
        setShow(false);
        window.location.reload()
    }
    const handleShow = () => setShow(true);

    //Código para la generacion del archivo csv
    const [dataList, setDataList] = useState([])
    const headers = [
        { label: "ID_ESTADO", key: "id_estado" },
        { label: "NOMBRE_ESTADO", key: "nombre_estado" },
        { label: "ID_DISTRITO_FEDERAL", key: "id_distrito_federal" },
        { label: "CABECERA_DISTRITAL_FEDERAL", key: "cabecera_distrital_federal" },
        { label: "ID_DISTRITO_LOCAL", key: "id_distrito_local" },
        { label: "CABECERA_DISTRITAL_LOCAL", key: "cabecera_distrital_local" },
        { label: "ID_MUNICIPIO_LOCAL", key: "id_municipio_local" },
        { label: "MUNICIPIO_LOCAL", key: "municipio_local" },
        { label: "SECCION", key: "seccion" },
        { label: "ID_CASILLA", key: "id_casilla" },
        { label: "TIPO_CASILLA", key: "tipo_casilla" },
        { label: "EXT_CONTIGUA", key: "ext_contigua" },
        { label: "FECHA_HORA_SALIDA", key: "salida" },
        { label: "FECHA_HORA_ENTRADA", key: "entrada" },
        { label: "FUNCIONARIO_ENTREGA_SALIDA", key: "funcionario_entrega_salida" },
        { label: "NOMBRE_FUNCIONARIO_ENTREGA_SALIDA", key: "nombre_funcionario_entrega_salida" },
        { label: "FUNCIONARIO_RECIBE_SALIDA", key: "funcionario_recibe_salida" },
        { label: "NOMBRE_FUNCIONARIO_RECIBE_SALIDA", key: "nombre_funcionario_recibe_salida" },
        { label: "FUNCIONARIO_ENTREGA_ENTRADA", key: "funcionario_entrega_entrada" },
        { label: "NOMBRE_FUNCIONARIO_ENTREGA_ENTRADA", key: "nombre_funcionario_entrega_entrada" },
        { label: "FUNCIONARIO_RECIBE_ENTRADA", key: "funcionario_recibe_entrada" },
        { label: "NOMBRE_FUNCIONARIO_RECIBE_ENTRADA", key: "nombre_funcionario_recibe_entrada" },
    ];

    //Metodo get simple
    const getData = () => {
        fetch(url + "/bitacora_paquetes_electorales", {
            method: "GET",
            headers:{
                "Content-Type": "application/json"
            }
        })
            .then(response => response.json())
            .then(result => {

                setDataList(result)
                console.log(result)
                
            })
            .catch(error => {
                console.log(error)
            })
    }

    //Metodo para obtener datos de la tabla y generar archivo csv
    const getDataByIdUsuario = () => {
        const data = {id_usuario:usuario.id}
        fetch(url + "/bitacora_paquetes_byidusuario", {
                method: "POST",
                headers:{
                    "Content-Type": "application/json"
                },
                body: JSON.stringify(data)
            })
            .then(response => response.json())
            .then(result => {
                setDataList(result)
                console.log(result)
                
            })
            .catch(error => {
                console.log(error)
            })
    }


    //Variables del documento
    const [fechaHoraSalida, setFechaHoraSalida] = useState()
    const [fechaHoraEntrada, setFechaHoraEntrada] = useState()

    const [funcionarioEntregaSalida, setFuncionarioEntregaSalida] = useState("VOCAL DE ORGANIZACION")
    const [nombreFuncionarioEntregaSalida, setNombreFuncionarioEntregaSalida] = useState()

    const [funcionarioRecibeSalida, setFuncionarioRecibeSalida] = useState("VOCAL DE ORGANIZACION")
    const [nombreFuncionarioRecibeSalida, setNombreFuncionarioRecibeSalida] = useState()

    const [funcionarioEntregaEntrada, setFuncionarioEntregaEntrada] = useState("VOCAL DE ORGANIZACIÓN")
    const [nombreFuncionarioEntregaEntrada, setNombreFuncionarioEntregaEntrada] = useState()

    const [funcionarioRecibeEntrada, setFuncionarioRecibeEntrada] = useState("VOCAL DE ORGANIZACION")
    const [nombreFuncionarioRecibeEntrada, setNombreFuncionarioRecibeEntrada] = useState()

    //Validacion de campos
    const [errorFuncionarioEntregaSalida, setErrorFuncionarioEntregaSalida] = useState(false)
    const [errorFuncionarioRecibeSalida, setErrorFuncionarioRecibeSalida] = useState(false)
    const [errorFuncionarioEntregaEntrada, setErrorFuncionarioEntregaEntrada] = useState(false)
    const [errorFuncionarioRecibeEntrada, setErrorFuncionarioRecibeEntrada] = useState(false)
    const regex = RegExp(/^[A-Za-z ]+$/)

    const handleEntregaSalida = (e) => {
        setNombreFuncionarioEntregaSalida(e.target.value)
        if(!regex.test(e.target.value)){
            setErrorFuncionarioEntregaSalida(true)
        }else{
            setErrorFuncionarioEntregaSalida(false)
        }
    }

    const handleRecibeSalida = (e) => {
        setNombreFuncionarioRecibeSalida(e.target.value)
        if(!regex.test(e.target.value)){
            setErrorFuncionarioRecibeSalida(true)
        }else{
            setErrorFuncionarioRecibeSalida(false)
        }
    }

    const handleEntregaEntrada = (e) => {
        setNombreFuncionarioEntregaEntrada(e.target.value)
        if(!regex.test(e.target.value)){
            setErrorFuncionarioEntregaEntrada(true)
        }else{
            setErrorFuncionarioEntregaEntrada(false)
        }
    }

    const handleRecibeEntrada = (e) => {
        setNombreFuncionarioRecibeEntrada(e.target.value)
        if(!regex.test(e.target.value)){
            setErrorFuncionarioRecibeEntrada(true)
        }else{
            setErrorFuncionarioRecibeEntrada(false)
        }
    }

    //Metodo para nuevo registro de bitacora de paquetes electorales
    const registrarBitacora = (e) => {
        e.preventDefault()

        const data = {
            id_usuario: usuario.id,
            casilla_id: selectedOption.value.id,
            fecha_hora_salida: fechaHoraSalida,
            fecha_hora_entrada: fechaHoraEntrada,
            funcionario_entrega_salida: funcionarioEntregaSalida,
            nombre_funcionario_entrega_salida: nombreFuncionarioEntregaSalida.toLocaleUpperCase(),
            funcionario_recibe_salida: funcionarioRecibeSalida,
            nombre_funcionario_recibe_salida: nombreFuncionarioRecibeSalida.toLocaleUpperCase(),
            funcionario_entrega_entrada: funcionarioEntregaEntrada,
            nombre_funcionario_entrega_entrada: nombreFuncionarioEntregaEntrada.toLocaleUpperCase(),
            funcionario_recibe_entrada: funcionarioRecibeEntrada,
            nombre_funcionario_recibe_entrada: nombreFuncionarioRecibeEntrada.toLocaleUpperCase()
        }

        if(!errorFuncionarioEntregaSalida && !errorFuncionarioRecibeSalida && !errorFuncionarioEntregaEntrada && !errorFuncionarioRecibeEntrada){
            console.log("form valido")
            fetch(url + "/registrar_bitacora", {
                method: "POST",
                headers:{
                    "Content-Type": "application/json"
                },
                body: JSON.stringify(data)
            })
            .then(response => response.json())
            .then(result => {
                setMensaje("Registro guardado con éxito")
                handleShow()
                console.log(result)
                
            })
            .catch(error => {
                setMensaje(error)
                handleShow()
                console.log(error)
            })

        }
    }

    //variables para casilla
    const [options, setOptions] = useState([]);
    const [selectedOption, setSelectedOption] = useState(null);
    const [loading, setLoading] = useState(true);

    //metodo para obtener casillas
    const getCasillas = () => {
        fetch(url+ "/get_casillas", {
            method: "GET",
            headers:{
                "Content-Type": "application/json"
            }
        })
            .then(response => response.json())
            .then(result => {
                const formattedOptions = result.map((item, index) => ({
                    value: item,
                    label: item.nombre_casilla
                }));
                setOptions(formattedOptions)
                setLoading(false)
                console.log(result)
                
            })
            .catch(error => {
                console.log(error)
            })
    }

    const SelectWithSearch = () => {
        const handleSelectChange = (selectedOption) => {
          setSelectedOption(selectedOption);
          console.log(selectedOption)
        };
      
        return (
          <div>
            {loading ? (
              <p>Cargando...</p>
            ) : (
              <Select
                value={selectedOption}
                onChange={handleSelectChange}
                options={options}
                placeholder="Buscar..."
              />
            )}
          </div>
        );
    };

    return(
        <>
        <Container>
            <h5>Bitácora de paquetes electorales</h5>
            <Row>
                <Col sm={10}>
                    <p>Ingrese los siguientes datos para el registro de la bitácora de entrada y salida de paquetes electorales</p>
                </Col>
                <Col sm={2}>
                    <CSVLink data={dataList} headers={headers} filename="BITACORA DE PAQUETES ELECTORALES">
                        <Button className="button-custom">
                            {loading ? 'Generando...' : 'Descargar'}
                        </Button>
                    </CSVLink>
                </Col>
            </Row>
            
            
            <Form onSubmit={registrarBitacora}>
            <Row className="mb-3">
                <Col xs lg="6">
                    <Card className="mt-1">
                        <Card.Body>
                            <Card.Text>
                                <Form.Group className="mb-4">
                                    <Form.Label>Casilla</Form.Label>
                                    <SelectWithSearch />
                                </Form.Group>
                                <Form.Group className="mb-4">
                                    <Form.Label>Registra la fecha y hora de la salida del paquete electoral</Form.Label>
                                    <Form.Control type="datetime-local" required onChange={(e)=>setFechaHoraSalida(e.target.value)}/>
                                </Form.Group>
                                <Form.Group className="mb-4">
                                    <Form.Label>Registra la fecha y hora de la entrada del paquete electoral</Form.Label>
                                    <Form.Control type="datetime-local" required onChange={(e)=>setFechaHoraEntrada(e.target.value)}/>
                                </Form.Group>
                                <Form.Group className="mb-4">
                                    <Form.Label>Funcionario que hace entrega del paquete electoral para su salida</Form.Label>
                                    <Form.Select onChange={(e)=>setFuncionarioEntregaSalida(e.target.value)}>
                                        <option value="VOCAL DE ORGANIZACION">VOCAL DE ORGANIZACIÓN</option>
                                        <option value="SUPERVISOR ELECTORAL">SUPERVISOR ELECTORAL</option>
                                        <option value="CAPACITADOR ASISTENTE ELECTORAL LOCAL">CAPACITADOR ASISTENTE ELECTORAL LOCAL</option>
                                    </Form.Select>
                                </Form.Group>
                                <Form.Group className="mb-4">
                                    <Form.Label>Nombre completo del funcionario que hace entrega del paquete electoral para su salida</Form.Label>
                                    <Form.Control required onChange={(e)=>handleEntregaSalida(e)} style={{ textTransform: 'uppercase'}}/>
                                    <Form.Text className="text-muted" id="error">
                                    Solo se permiten letras. Sin acentos ni caracteres numéricos o especiales.
                                    </Form.Text>
                                    {errorFuncionarioEntregaSalida && <p style={{color: "red"}}>El texto es inválido</p>}
                                </Form.Group>
                                <Form.Group className="mb-4">
                                    <Form.Label>Funcionario que recibe el paquete electoral para su salida</Form.Label>
                                    <Form.Select onChange={(e)=>setFuncionarioRecibeSalida(e.target.value)}>
                                        <option value="VOCAL DE ORGANIZACION">VOCAL DE ORGANIZACIÓN</option>
                                        <option value="SUPERVISOR ELECTORAL">SUPERVISOR ELECTORAL</option>
                                        <option value="CAPACITADOR ASISTENTE ELECTORAL LOCAL">CAPACITADOR ASISTENTE ELECTORAL LOCAL</option>
                                    </Form.Select>
                                </Form.Group>
                                <Form.Group className="mb-4">
                                    <Form.Label>Nombre completo del funcionario que recibe el paquete electoral para su salida</Form.Label>
                                    <Form.Control required onChange={(e)=>handleRecibeSalida(e)} style={{ textTransform: 'uppercase'}}/>
                                    <Form.Text className="text-muted" id="error">
                                    Solo se permiten letras. Sin acentos ni caracteres numéricos o especiales.
                                    </Form.Text>
                                    {errorFuncionarioRecibeSalida && <p style={{color: "red"}}>El texto es inválido</p>}
                                </Form.Group>
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Col>

                <Col xs lg="6">
                    <Card className="mt-1">
                        <Card.Body>
                            <Card.Text>
                                <Form.Group className="mb-4">
                                    <Form.Label>Funcionario que entrega el paquete electoral para su entrada</Form.Label>
                                    <Form.Select onChange={(e)=>setFuncionarioEntregaEntrada(e.target.value)}>
                                        <option value="VOCAL DE ORGANIZACION">VOCAL DE ORGANIZACIÓN</option>
                                        <option value="SUPERVISOR ELECTORAL">SUPERVISOR ELECTORAL</option>
                                        <option value="CAPACITADOR ASISTENTE ELECTORAL LOCAL">CAPACITADOR ASISTENTE ELECTORAL LOCAL</option>
                                    </Form.Select>
                                </Form.Group>
                                <Form.Group className="mb-4">
                                    <Form.Label>Nombre completo del funcionario que entrega el paquete electoral para su entrada</Form.Label>
                                    <Form.Control required onChange={(e)=>handleEntregaEntrada(e)} style={{ textTransform: 'uppercase'}}/>
                                    <Form.Text className="text-muted" id="error">
                                    Solo se permiten letras. Sin acentos ni caracteres numéricos o especiales.
                                    </Form.Text>
                                    {errorFuncionarioEntregaEntrada && <p style={{color: "red"}}>El texto es inválido</p>}
                                </Form.Group>
                                <Form.Group className="mb-4">
                                    <Form.Label>Funcionario que recibe el paquete electoral para su entrada</Form.Label>
                                    <Form.Select onChange={(e)=>setFuncionarioRecibeEntrada(e.target.value)}>
                                        <option value="VOCAL DE ORGANIZACION">VOCAL DE ORGANIZACIÓN</option>
                                        <option value="SUPERVISOR ELECTORAL">SUPERVISOR ELECTORAL</option>
                                        <option value="CAPACITADOR ASISTENTE ELECTORAL LOCAL">CAPACITADOR ASISTENTE ELECTORAL LOCAL</option>
                                    </Form.Select>
                                </Form.Group>
                                <Form.Group className="mb-4">
                                    <Form.Label>Nombre completo del funcionario que recibe el paquete electoral para su entrada</Form.Label>
                                    <Form.Control required onChange={(e)=>handleRecibeEntrada(e)} style={{ textTransform: 'uppercase'}}/>
                                    <Form.Text className="text-muted" id="error">
                                    Solo se permiten letras. Sin acentos ni caracteres numéricos o especiales.
                                    </Form.Text>
                                    {errorFuncionarioRecibeEntrada && <p style={{color: "red"}}>El texto es inválido</p>}
                                </Form.Group>
                                
                            </Card.Text>
                        </Card.Body>
                    </Card>
                    <div className="d-grid gap-2 mt-3">
                        <Button type="submit" className="button-custom">Guardar</Button>
                    </div>
                </Col>
            </Row>
            </Form>
        </Container>
        <Modal
            show={show}
            onHide={handleClose}
            backdrop="static"
            keyboard={false}
        >
            <Modal.Header closeButton>
                <Modal.Title></Modal.Title>
            </Modal.Header>
            <Modal.Body>
            {mensaje}
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={handleClose}>
                    Cerrar
                </Button>
            </Modal.Footer>
        </Modal>
        </>
    )

}

export default BitacoraPaquetes