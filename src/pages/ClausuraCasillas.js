import React, { useEffect, useState } from "react"
import { Container } from "react-bootstrap";
import {Row, Card, Button} from "react-bootstrap";
import {Col, Form} from "react-bootstrap";
import { CSVLink } from "react-csv";
import { Modal } from "react-bootstrap";
import Select from 'react-select';
import {config} from "../Constants"

const ClausuraCasillas = () => {
    const url = config.url.BASE_URL;

    useEffect(() => {
        if(usuario.rol === "Admin"){
            console.log(usuario.rol)
            getData()
            getCasillas()
        } else if(usuario.rol === "usuario"){
            console.log(usuario.rol)
            getDataByIdUsuario()
            getCasillas()
        }
      }, [])

    let usuario = {}
    if(localStorage.getItem("usuario")){
        usuario = JSON.parse(localStorage.getItem("usuario"))
    }

    const [mensaje, setMensaje] = useState("")
    const [show, setShow] = useState(false);
    const handleClose = () => {
        setShow(false);
        window.location.reload()
    }
    const handleShow = () => setShow(true);

    //Código para la generacion del archivo csv
    const [dataList, setDataList] = useState([])
    const headers = [
        { label: "ID_ESTADO", key: "id_estado" },
        { label: "NOMBRE_ESTADO", key: "nombre_estado" },
        { label: "ID_DISTRITO_FEDERAL", key: "id_distrito_federal" },
        { label: "CABECERA_DISTRITAL_FEDERAL", key: "cabecera_distrital_federal" },
        { label: "ID_DISTRITO_LOCAL", key: "id_distrito_local" },
        { label: "CABECERA_DISTRITAL_LOCAL", key: "cabecera_distrital_local" },
        { label: "ID_MUNICIPIO_LOCAL", key: "id_municipio_local" },
        { label: "MUNICIPIO_LOCAL", key: "municipio_local" },
        { label: "SECCION", key: "seccion" },
        { label: "ID_CASILLA", key: "id_casilla" },
        { label: "TIPO_CASILLA", key: "tipo_casilla" },
        { label: "EXT_CONTIGUA", key: "ext_contigua" },
        { label: "FECHA_HORA_CLAUSURA", key: "clausura" },
        { label: "PRESENCIA_PRESIDENTE", key: "presencia_presidente" },
        { label: "PRESENCIA_SECRETARIO", key: "presencia_secretario" },
        { label: "PRESENCIA_SEGUNDO_SECRETARIO", key: "presencia_segundo_secretario" },
        { label: "PRESENCIA_PRIMER_ESCRUTADOR", key: "presencia_primer_escrutador" },
        { label: "PRESENCIA_SEGUNDO_ESCRUTADOR", key: "presencia_segundo_escrutador" },
        { label: "PRESENCIA_TERCER_ESCRUTADOR", key: "presencia_tercer_escrutador" },
        { label: "CONSTANCIA", key: "constancia" },
        { label: "ESTATUS_CASILLA_INSTALADA", key: "estatus_casilla_instalada" },
        { label: "MOTIVO_CAMBIO_DOMICILIO", key: "motivo_cambio_domicilio" },
    ];

    //Metodo get simple
    const getData = () => {
        fetch(url + "/clausura_casillas", {
            method: "GET",
            headers:{
                "Content-Type": "application/json"
            }
        })
            .then(response => response.json())
            .then(result => {

                setDataList(result)
                console.log(result)
                
            })
            .catch(error => {
                console.log(error)
            })
    }

    //Metodo para obtener datos de la tabla y generar archivo csv
    const getDataByIdUsuario = () => {
        const data = {id_usuario:usuario.id}
        fetch(url + "/clausura_casillas_byidusuario", {
                method: "POST",
                headers:{
                    "Content-Type": "application/json"
                },
                body: JSON.stringify(data)
            })
            .then(response => response.json())
            .then(result => {
                setDataList(result)
                console.log(result)
                
            })
            .catch(error => {
                console.log(error)
            })
    }


    //Variables del documento
    const [fechaHoraClausura, setFechaHoraClausura] = useState()


    const [presenciaPresidente, setPresenciaPresidente] = useState("S")
    const [presenciaSecretario, setPresenciaSecretario] = useState("S")
    const [presenciaSegundoSecretario, setPresenciaSegundoSecretario] = useState("S")
    const [presenciaPrimerEscrutador, setPresenciaPrimerEscrutador] = useState("S")
    const [presenciaSegundoEscrutador, setPresenciaSegundoEscrutador] = useState("S")
    const [presenciaTercerEscrutador, setPresenciaTercerEscrutador] = useState("S")

    const [constancia, setConstancia] = useState("S")
    const [estatusCasillaInstalada, setEstatusCasillaInstalada] = useState("S")
    const [motivoCambioDomicilio, setMotivoCambioDomicilio] = useState("SIN CAMBIO")

    //Validacion de campos
    const [errorMotivoCambioDomicilio, setErrorMotivoCambioDomicilio] = useState(false)
    const regex = RegExp(/^[A-Za-z ]+$/)

    const handleMotivoCambioDomicilio = (e) => {
        setMotivoCambioDomicilio(e.target.value)

        if(e.target.value != ""){
            if(!regex.test(e.target.value)){
                setErrorMotivoCambioDomicilio(true)
            }else{
                setErrorMotivoCambioDomicilio(false)
            }
        }else{
            setMotivoCambioDomicilio("SIN CAMBIO")
            setErrorMotivoCambioDomicilio(false)
        }
        
    }

    //Metodo para nuevo registro de clausura de casillas
    const registrarClausuraCasillas = (e) => {
        e.preventDefault()

        const data = {
            id_usuario: usuario.id,
            casilla_id: selectedOption.value.id,
            fecha_hora_clausura: fechaHoraClausura,
            presencia_presidente: presenciaPresidente,
            presencia_secretario: presenciaSecretario,
            presencia_segundo_secretario: presenciaSegundoSecretario,
            presencia_primer_escrutador: presenciaPrimerEscrutador,
            presencia_segundo_escrutador: presenciaSegundoEscrutador,
            presencia_tercer_escrutador: presenciaTercerEscrutador,
            constancia: constancia,
            estatus_casilla_instalada: estatusCasillaInstalada,
            motivo_cambio_domicilio: motivoCambioDomicilio.toLocaleUpperCase()
        }

        if(!errorMotivoCambioDomicilio){
            console.log("form valido")
            fetch(url + "/registrar_clausura_casillas", {
                method: "POST",
                headers:{
                    "Content-Type": "application/json"
                },
                body: JSON.stringify(data)
            })
            .then(response => response.json())
            .then(result => {
                setMensaje("Registro guardado con éxito")
                handleShow()
                console.log(result)
                
            })
            .catch(error => {
                setMensaje(error)
                handleShow()
                console.log(error)
            })

        }
    }

    //variables para casilla
    const [options, setOptions] = useState([]);
    const [selectedOption, setSelectedOption] = useState(null);
    const [loading, setLoading] = useState(true);

    //metodo para obtener casillas
    const getCasillas = () => {
        fetch(url + "/get_casillas", {
            method: "GET",
            headers:{
                "Content-Type": "application/json"
            }
        })
            .then(response => response.json())
            .then(result => {
                const formattedOptions = result.map((item, index) => ({
                    value: item,
                    label: item.nombre_casilla
                }));
                setOptions(formattedOptions)
                setLoading(false)
                console.log(result)
                
            })
            .catch(error => {
                console.log(error)
            })
    }

    const SelectWithSearch = () => {
        const handleSelectChange = (selectedOption) => {
          setSelectedOption(selectedOption);
          console.log(selectedOption)
        };
      
        return (
          <div>
            {loading ? (
              <p>Cargando...</p>
            ) : (
              <Select
                value={selectedOption}
                onChange={handleSelectChange}
                options={options}
                placeholder="Buscar..."
              />
            )}
          </div>
        );
    };

    return(
        <>
        <Container>
            <h5>Clausura de casillas electorales</h5>
            <Row>
                <Col sm={10}>
                    <p>Ingrese los siguientes datos para el registro de las acciones de la clausura de casillas</p>
                </Col>
                <Col sm={2}>
                    <CSVLink data={dataList} headers={headers} filename="CLAUSURA DE CASILLAS">
                        <Button className="button-custom">
                            {loading ? 'Generando...' : 'Descargar'}
                        </Button>
                    </CSVLink>
                </Col>
            </Row>
            
            
            <Form onSubmit={registrarClausuraCasillas}>
            <Row className="mb-3">
                <Col xs lg="6">
                    <Card className="mt-1">
                        <Card.Body>
                            <Card.Text>
                                <Form.Group className="mb-4">
                                    <Form.Label>Casilla</Form.Label>
                                    <SelectWithSearch />
                                </Form.Group>
                                <Form.Group className="mb-4">
                                    <Form.Label>Registra la fecha y hora en que se realiza la clausura de casilla</Form.Label>
                                    <Form.Control type="datetime-local" required onChange={(e)=>setFechaHoraClausura(e.target.value)}/>
                                </Form.Group>
                                <Form.Group className="mb-4">
                                    <Form.Label>Presencia del presidente al momento de la clausura</Form.Label>
                                    <Form.Select onChange={(e)=>setPresenciaPresidente(e.target.value)}>
                                        <option value="S">SI</option>
                                        <option value="N">NO</option>
                                    </Form.Select>
                                </Form.Group>
                                <Form.Group className="mb-4">
                                    <Form.Label>Presencia del secretario al momento de la clausura</Form.Label>
                                    <Form.Select onChange={(e)=>setPresenciaSecretario(e.target.value)}>
                                        <option value="S">SI</option>
                                        <option value="N">NO</option>
                                    </Form.Select>
                                </Form.Group>
                                <Form.Group className="mb-4">
                                    <Form.Label>Presencia del segundo secretario al momento de la clausura</Form.Label>
                                    <Form.Select onChange={(e)=>setPresenciaSegundoSecretario(e.target.value)}>
                                        <option value="S">SI</option>
                                        <option value="N">NO</option>
                                    </Form.Select>
                                </Form.Group>
                                <Form.Group className="mb-4">
                                    <Form.Label>Presencia del primer escrutador al momento de la clausura</Form.Label>
                                    <Form.Select onChange={(e)=>setPresenciaPrimerEscrutador(e.target.value)}>
                                        <option value="S">SI</option>
                                        <option value="N">NO</option>
                                    </Form.Select>
                                </Form.Group>
                                <Form.Group className="mb-4">
                                    <Form.Label>Presencia del segundo escrutador al momento de la clausura</Form.Label>
                                    <Form.Select onChange={(e)=>setPresenciaSegundoEscrutador(e.target.value)}>
                                        <option value="S">SI</option>
                                        <option value="N">NO</option>
                                    </Form.Select>
                                </Form.Group>
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Col>

                <Col xs lg="6">
                    <Card className="mt-1">
                        <Card.Body>
                            <Card.Text>
                                <Form.Group className="mb-4">
                                    <Form.Label>Presencia del tercer escrutador al momento de la clausura</Form.Label>
                                    <Form.Select onChange={(e)=>setPresenciaTercerEscrutador(e.target.value)}>
                                        <option value="S">SI</option>
                                        <option value="N">NO</option>
                                    </Form.Select>
                                </Form.Group>
                                <Form.Group className="mb-4">
                                    <Form.Label>Generación de constancia por cierre de casilla</Form.Label>
                                    <Form.Select onChange={(e)=>setConstancia(e.target.value)}>
                                        <option value="S">SI</option>
                                        <option value="N">NO</option>
                                    </Form.Select>
                                </Form.Group>
                                <Form.Group className="mb-4">
                                    <Form.Label>¿Se instaló la casilla?</Form.Label>
                                    <Form.Select onChange={(e)=>setEstatusCasillaInstalada(e.target.value)}>
                                        <option value="S">SI</option>
                                        <option value="N">NO</option>
                                    </Form.Select>
                                </Form.Group>
                                <Form.Group className="mb-4">
                                    <Form.Label>En caso de cambio de domicilio, describe el motivo por el que se realizó el cambio de la casilla</Form.Label>
                                    <Form.Control as="textarea" onChange={(e)=>handleMotivoCambioDomicilio(e)} style={{ textTransform: 'uppercase'}}/>
                                    <Form.Text className="text-muted" id="error">
                                    Solo se permiten letras. Sin acentos ni caracteres numéricos o especiales.
                                    </Form.Text>
                                    {errorMotivoCambioDomicilio && <p style={{color: "red"}}>El texto es inválido</p>}
                                </Form.Group>
                            </Card.Text>
                        </Card.Body>
                    </Card>
                    <div className="d-grid gap-2 mt-3">
                        <Button type="submit" className="button-custom">Guardar</Button>
                    </div>
                </Col>
            </Row>
            </Form>
        </Container>
        <Modal
            show={show}
            onHide={handleClose}
            backdrop="static"
            keyboard={false}
        >
            <Modal.Header closeButton>
                <Modal.Title></Modal.Title>
            </Modal.Header>
            <Modal.Body>
            {mensaje}
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={handleClose}>
                    Cerrar
                </Button>
            </Modal.Footer>
        </Modal>
        </>
    )

}

export default ClausuraCasillas