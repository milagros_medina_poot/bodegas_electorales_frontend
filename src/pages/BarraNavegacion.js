import React from "react"
import { Outlet } from "react-router-dom"
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import { Link, useLocation } from 'react-router-dom';
import '../App.css';
import { Container } from "react-bootstrap";
import { Button } from "react-bootstrap";

const BarraNavegacion = ()  =>{

    const location = useLocation();

    let usuario = {}
    if(localStorage.getItem("usuario")){
        usuario = JSON.parse(localStorage.getItem("usuario"))
    }

    const cerrarSesion = () => {
        localStorage.removeItem("token")
        //window.location.reload()
        //navigate("/perfil")
        window.location.href = '/'
    }

    return(
        <div>
            <Navbar  bg="myColor" data-bs-theme="dark" sticky="top"  expand="lg">
                <Container fluid>
                    <Navbar.Brand as={Link} to="/">IEQROO | Sistema para Registro de Bitácoras</Navbar.Brand>
                    <Navbar.Collapse className="justify-content-end">
                        <Navbar.Text>
                        {usuario.nombre}
                        </Navbar.Text>
                    </Navbar.Collapse>
                </Container>
            </Navbar>

            <div className="sidebar">
                <Nav className="flex-column">
                    <Nav.Link href="perfil" className={location.pathname === '/perfil' ? 'active' : ''}>Mi Perfil</Nav.Link>
                    <Nav.Link href="apertura_cierre" className={location.pathname === '/apertura_cierre' ? 'active' : ''}>1. Apertura y cierre</Nav.Link>
                    <Nav.Link href="bitacora_paquetes" className={location.pathname === '/bitacora_paquetes' ? 'active' : ''}>2. Bitácora de paquetes electorales</Nav.Link>
                    <Nav.Link href="clausura_casillas" className={location.pathname === '/clausura_casillas' ? 'active' : ''}>3. Clausura de casillas</Nav.Link>
                    <Nav.Link href="detalle_presencia_apertura_bodega" className={location.pathname === '/detalle_presencia_apertura_bodega' ? 'active' : ''}>4. Detalle presencia de apertura de bodega</Nav.Link>
                    <Nav.Link href="funcionamiento_cryt_dat" className={location.pathname === '/funcionamiento_cryt_dat' ? 'active' : ''}>5. Funcionamiento de CRYT y DAT</Nav.Link>
                    <Nav.Link href="presencia_apertura_bodegas" className={location.pathname === '/presencia_apertura_bodegas' ? 'active' : ''}>6. Presencia de apertura de bodegas</Nav.Link>
                    <Nav.Link href="presencia_partidos_clausura" className={location.pathname === '/presencia_partidos_clausura' ? 'active' : ''}>7. Presencia de partidos en la clausura</Nav.Link>
                    <Nav.Link href="recepcion_paquetes" className={location.pathname === '/recepcion_paquetes' ? 'active' : ''}>8. Recepción de paquetes</Nav.Link>
                    
                </Nav>
                <Button  onClick={cerrarSesion} className="button-custom m-2" >Cerrar sesión</Button>
            </div>
            <div className="content">
                <Outlet/>
            </div>
            
        </div>
        
    )
}

export default BarraNavegacion;