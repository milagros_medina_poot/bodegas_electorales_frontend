import React from "react"
import AperturaCierre from "./AperturaCierre";
import BitacoraPaquetes from "./BitacoraPaquetes";
import ClausuraCasillas from "./ClausuraCasillas";
import DetallePresenciaAperturaBodega from "./DetallePresenciaAperturaBodega";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import FuncionamientoCrytDat from "./FuncionamientoCrytDat";
import PresenciaAperturaBodegas from "./PresenciaAperturaBodegas";
import RecepcionPaquetes from "./RecepcionPaquetes";
import PresenciaPartidosClausura from "./PresenciaPartidosClausura";
import Perfil from "./Perfil";
import BarraNavegacion from "./BarraNavegacion";

const Home = () => {

    return(
        <>
            <BrowserRouter>
                <Routes>
                    <Route path="/" element={<BarraNavegacion />}>
                        <Route index element={<Perfil />} />
                        <Route path="perfil" element={<Perfil />} />
                        <Route path="apertura_cierre" element={<AperturaCierre />} />
                        <Route path="bitacora_paquetes" element={<BitacoraPaquetes />} />
                        <Route path="clausura_casillas" element={<ClausuraCasillas />} />
                        <Route path="detalle_presencia_apertura_bodega" element={<DetallePresenciaAperturaBodega />} />
                        <Route path="funcionamiento_cryt_dat" element={<FuncionamientoCrytDat />} />
                        <Route path="presencia_apertura_bodegas" element={<PresenciaAperturaBodegas />} />
                        <Route path="presencia_partidos_clausura" element={<PresenciaPartidosClausura />} />
                        <Route path="recepcion_paquetes" element={<RecepcionPaquetes />} />
                    </Route>
                </Routes>
            </BrowserRouter>            
        </>
        
    )
}
export default Home