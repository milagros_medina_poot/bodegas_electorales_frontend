import React, { useEffect, useState } from "react"
import { Container } from "react-bootstrap";
import {Row, Card, Button} from "react-bootstrap";
import {Col, Form} from "react-bootstrap";
import { CSVLink } from "react-csv";
import { Modal } from "react-bootstrap";
import Select from 'react-select';
import {config} from "../Constants"

const url = config.url.BASE_URL;

const PresenciaPartidosClausura = () => {
    let usuario = {}
    if(localStorage.getItem("usuario")){
        usuario = JSON.parse(localStorage.getItem("usuario"))
    }

    const [mensaje, setMensaje] = useState("")
    const [show, setShow] = useState(false);
    const handleClose = () => {
        setShow(false);
        window.location.reload()
    }
    const handleShow = () => setShow(true);

    //Código para la generacion del archivo csv
    const [dataList, setDataList] = useState([])
    const headers = [
        { label: "ID_ESTADO", key: "id_estado" },
        { label: "NOMBRE_ESTADO", key: "nombre_estado" },
        { label: "ID_DISTRITO_FED", key: "id_distrito_federal" },
        { label: "CABECERA_DISTRITAL_FED", key: "cabecera_distrital_federal" },
        { label: "ID_DISTRITO_LOC", key: "id_distrito_local" },
        { label: "CABECERA_DISTRITAL_LOC", key: "cabecera_distrital_local" },
        { label: "ID_MUNICIPIO_", key: "id_municipio_local" },
        { label: "MUNICIPIO", key: "municipio_local" },

        { label: "SECCION", key: "seccion" },
        { label: "ID_CASILLA", key: "id_casilla" },
        { label: "TIPO_CASILLA", key: "tipo_casilla" },
        { label: "EXT_CONTIGUA", key: "ext_contigua" },

        { label: "ID_PARTIDO1_CI", key: "id_partido1" },
        { label: "PARTIDO_POLITICO1_CI", key: "partido_politico1" },
        { label: "PRESENCIA_REPRESENTANTE", key: "presencia_representante1" },

        { label: "ID_PARTIDO2_CI", key: "id_partido2" },
        { label: "PARTIDO_POLITICO2_CI", key: "partido_politico2" },
        { label: "PRESENCIA_REPRESENTANTE", key: "presencia_representante2" },

        { label: "ID_PARTIDO3_CI", key: "id_partido3" },
        { label: "PARTIDO_POLITICO3_CI", key: "partido_politico3" },
        { label: "PRESENCIA_REPRESENTANTE", key: "presencia_representante3" },

        { label: "ID_PARTIDO4_CI", key: "id_partido4" },
        { label: "PARTIDO_POLITICO4_CI", key: "partido_politico4" },
        { label: "PRESENCIA_REPRESENTANTE", key: "presencia_representante4" },

        { label: "ID_PARTIDO5_CI", key: "id_partido5" },
        { label: "PARTIDO_POLITICO5_CI", key: "partido_politico5" },
        { label: "PRESENCIA_REPRESENTANTE", key: "presencia_representante5" },

        { label: "ID_PARTIDO6_CI", key: "id_partido6" },
        { label: "PARTIDO_POLITICO6_CI", key: "partido_politico6" },
        { label: "PRESENCIA_REPRESENTANTE", key: "presencia_representante6" },

        { label: "ID_PARTIDO7_CI", key: "id_partido7" },
        { label: "PARTIDO_POLITICO7_CI", key: "partido_politico7" },
        { label: "PRESENCIA_REPRESENTANTE", key: "presencia_representante7" },

        { label: "ID_PARTIDO8_CI", key: "id_partido8" },
        { label: "PARTIDO_POLITICO8_CI", key: "partido_politico8" },
        { label: "PRESENCIA_REPRESENTANTE", key: "presencia_representante8" },

        { label: "ID_PARTIDO_CI", key: "id_partido9" },
        { label: "PARTIDO_POLITICO_CI", key: "partido_politico9" },
        { label: "PRESENCIA_REPRESENTANTE", key: "presencia_representante9" },
    ];

    //Metodo para obtener datos de la tabla y generar archivo csv
    const getDataByIdUsuario = () => {
        const data = {id_usuario:usuario.id}
        fetch(url + "/presencia_partidos_clausura_byidusuario", {
                method: "POST",
                headers:{
                    "Content-Type": "application/json"
                },
                body: JSON.stringify(data)
            })
            .then(response => response.json())
            .then(result => {
                setDataList(result)
                console.log(result)
                
            })
            .catch(error => {
                console.log(error)
            })
    }

    //Metodo get simple
    const getData = () => {
        fetch(url + "/presencia_partidos_clausura", {
            method: "GET",
            headers:{
                "Content-Type": "application/json"
            }
        })
            .then(response => response.json())
            .then(result => {

                setDataList(result)
                console.log(result)
                
            })
            .catch(error => {
                console.log(error)
            })
    }


    //Variables del documento
    const [presenciaPAN, setPresenciaPAN] = useState("S")
    const [presenciaPRI, setPresenciaPRI] = useState("S")
    const [presenciaPRD, setPresenciaPRD] = useState("S")
    const [presenciaPVEM, setPresenciaPVEM] = useState("S")
    const [presenciaPT, setPresenciaPT] = useState("S")
    const [presenciaMC, setPresenciaMC] = useState("S")
    const [presenciaMORENA, setPresenciaMORENA] = useState("S")
    const [presenciaMAS, setPresenciaMAS] = useState("S")
    const [presenciaCI, setPresenciaCI] = useState("S")

    //Metodo para nuevo registro de recepcion de paquetes
    const registrarData = (e) => {
        e.preventDefault()

        const data = {
            id_usuario: usuario.id,
            casilla_id: selectedOption.value.id,

            id_partido1: "PAN",
            partido_politico1: "PARTIDO ACCION NACIONAL",
            presencia_representante1: presenciaPAN,
            id_partido2: "PRI",
            partido_politico2: "PARTIDO REVOLUCIONARIO INSTITUCIONAL",
            presencia_representante2: presenciaPRI,
            id_partido3: "PRD",
            partido_politico3: "PARTIDO DE LA REVOLUCIÓN DEMOCRÁTICA",
            presencia_representante3: presenciaPRD,
            id_partido4: "PVEM",
            partido_politico4: "PARTIDO VERDE ECOLOGISTA DE MÉXICO",
            presencia_representante4: presenciaPVEM,
            id_partido5: "PT",
            partido_politico5: "PARTIDO DEL TRABAJO",
            presencia_representante5: presenciaPT,
            id_partido6: "MC",
            partido_politico6: "MOVIMIENTO CIUDADANO",
            presencia_representante6: presenciaMC,
            id_partido7: "MORENA",
            partido_politico7: "MOVIMIENTO DE REGENERACIÓN NACIONAL",
            presencia_representante7: presenciaMORENA,
            id_partido8: "MÁS",
            partido_politico8: "MÁS, MÁS APOYO SOCIAL",
            presencia_representante8: presenciaMAS,
            id_partido9: "CAN IND",
            partido_politico9: "DANIEL CRUZ MARTINEZ",
            presencia_representante9: presenciaCI
        }

        fetch(url + "/registrar_presencia_partidos_clausura", {
                method: "POST",
                headers:{
                    "Content-Type": "application/json"
                },
                body: JSON.stringify(data)
            })
            .then(response => response.json())
            .then(result => {
                setMensaje("Registro guardado con éxito")
                handleShow()
                console.log(result)
                
            })
            .catch(error => {
                setMensaje(error)
                handleShow()
                console.log(error)
            })
    }

    useEffect(() => {
        if(usuario.rol === "Admin"){
            console.log(usuario.rol)
            getData()
            getCasillas()
        } else if(usuario.rol === "usuario"){
            console.log(usuario.rol)
            getDataByIdUsuario()
            getCasillas()
        }
        
    }, [])

    //variables para casilla
    const [options, setOptions] = useState([]);
    const [selectedOption, setSelectedOption] = useState(null);
    const [loading, setLoading] = useState(true);

    //metodo para obtener casillas
    const getCasillas = () => {
        fetch(url + "/get_casillas", {
            method: "GET",
            headers:{
                "Content-Type": "application/json"
            }
        })
            .then(response => response.json())
            .then(result => {
                const formattedOptions = result.map((item, index) => ({
                    value: item,
                    label: item.nombre_casilla
                }));
                setOptions(formattedOptions)
                setLoading(false)
                console.log(result)
                
            })
            .catch(error => {
                console.log(error)
            })
    }

    const SelectWithSearch = () => {
        const handleSelectChange = (selectedOption) => {
          setSelectedOption(selectedOption);
          console.log(selectedOption)
        };
      
        return (
          <div>
            {loading ? (
              <p>Cargando...</p>
            ) : (
              <Select
                required
                value={selectedOption}
                onChange={handleSelectChange}
                options={options}
                placeholder="Buscar..."
              />
            )}
          </div>
        );
    };


    return(
        <>
        <Container>
            <h5>Presencia de partidos en la clausura</h5>
            <Row>
                <Col sm={10}>
                <p>Ingrese los siguientes datos para el registro de la información de los representantes de partidos que asistieron a la clausura de casilla</p>
                </Col>
                <Col sm={2}>
                <CSVLink data={dataList} headers={headers} filename="PRESENCIA DE PARTIDOS EN LA CLAUSURA">
                        <Button className="button-custom">
                            {loading ? 'Generando...' : 'Descargar'}
                        </Button>
                    </CSVLink>
                </Col>
            </Row>


            <Form onSubmit={registrarData}>
            <Row className="justify-content-md-center mb-3">
                <Col xs lg="8">
                    <Card className="mt-1">
                        <Card.Body>
                            <Card.Text>
                                <Form.Group className="mb-4">
                                    <Form.Label>Casilla</Form.Label>
                                    <SelectWithSearch />
                                </Form.Group>
                                <Form.Group className="mb-4">
                                    <Form.Label>Presencia del representante del PAN</Form.Label>
                                    <Form.Select onChange={(e)=>setPresenciaPAN(e.target.value)}>
                                        <option value="S">SI</option>
                                        <option value="N">NO</option>
                                    </Form.Select>
                                </Form.Group>
                                <Form.Group className="mb-4">
                                    <Form.Label>Presencia del representante del PRI</Form.Label>
                                    <Form.Select onChange={(e)=>setPresenciaPRI(e.target.value)}>
                                        <option value="S">SI</option>
                                        <option value="N">NO</option>
                                    </Form.Select>
                                </Form.Group>
                                <Form.Group className="mb-4">
                                    <Form.Label>Presencia del representante del PRD</Form.Label>
                                    <Form.Select onChange={(e)=>setPresenciaPRD(e.target.value)}>
                                        <option value="S">SI</option>
                                        <option value="N">NO</option>
                                    </Form.Select>
                                </Form.Group>
                                <Form.Group className="mb-4">
                                    <Form.Label>Presencia del representante del PVEM</Form.Label>
                                    <Form.Select onChange={(e)=>setPresenciaPVEM(e.target.value)}>
                                        <option value="S">SI</option>
                                        <option value="N">NO</option>
                                    </Form.Select>
                                </Form.Group>
                                <Form.Group className="mb-4">
                                    <Form.Label>Presencia del representante del PT</Form.Label>
                                    <Form.Select onChange={(e)=>setPresenciaPT(e.target.value)}>
                                        <option value="S">SI</option>
                                        <option value="N">NO</option>
                                    </Form.Select>
                                </Form.Group>
                                <Form.Group className="mb-4">
                                    <Form.Label>Presencia del representante de MC</Form.Label>
                                    <Form.Select onChange={(e)=>setPresenciaMC(e.target.value)}>
                                        <option value="S">SI</option>
                                        <option value="N">NO</option>
                                    </Form.Select>
                                </Form.Group>
                                <Form.Group className="mb-4">
                                    <Form.Label>Presencia del representante de MORENA</Form.Label>
                                    <Form.Select onChange={(e)=>setPresenciaMORENA(e.target.value)}>
                                        <option value="S">SI</option>
                                        <option value="N">NO</option>
                                    </Form.Select>
                                </Form.Group>
                                <Form.Group className="mb-4">
                                    <Form.Label>Presencia del representante de MÁS</Form.Label>
                                    <Form.Select onChange={(e)=>setPresenciaMAS(e.target.value)}>
                                        <option value="S">SI</option>
                                        <option value="N">NO</option>
                                    </Form.Select>
                                </Form.Group>
                                <Form.Group className="mb-4">
                                    <Form.Label>Presencia del representante de Candidatura Independiente</Form.Label>
                                    <Form.Select onChange={(e)=>setPresenciaCI(e.target.value)}>
                                        <option value="S">SI</option>
                                        <option value="N">NO</option>
                                    </Form.Select>
                                </Form.Group>
                            </Card.Text>
                        </Card.Body>
                    </Card>
                    <div className="d-grid gap-2 mt-3">
                        <Button type="submit" className="button-custom">Guardar</Button>
                    </div>
                </Col>
            </Row>
            </Form>
        </Container>
        <Modal
            show={show}
            onHide={handleClose}
            backdrop="static"
            keyboard={false}
        >
            <Modal.Header closeButton>
                <Modal.Title></Modal.Title>
            </Modal.Header>
            <Modal.Body>
            {mensaje}
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={handleClose}>
                    Cerrar
                </Button>
            </Modal.Footer>
        </Modal>
        </>
    )

}

export default PresenciaPartidosClausura