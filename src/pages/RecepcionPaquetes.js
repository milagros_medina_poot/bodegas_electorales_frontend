import React, { useEffect, useState } from "react"
import { Container } from "react-bootstrap";
import {Row, Card, Button} from "react-bootstrap";
import {Col, Form} from "react-bootstrap";
import { CSVLink, CSVDownload } from "react-csv";
import { Modal } from "react-bootstrap";
import Select from 'react-select';
import {config} from "../Constants"

const url = config.url.BASE_URL;

const RecepcionPaquetes = () => {
    
    let usuario = {}
    if(localStorage.getItem("usuario")){
        usuario = JSON.parse(localStorage.getItem("usuario"))
    }

    const [mensaje, setMensaje] = useState("")
    const [show, setShow] = useState(false);
    const handleClose = () => {
        setShow(false);
        window.location.reload()
    }
    const handleShow = () => setShow(true);

    //Código para la generacion del archivo csv
    const [dataList, setDataList] = useState([])
    const headers = [
        { label: "ID_ESTADO", key: "id_estado" },
        { label: "NOMBRE_ESTADO", key: "nombre_estado" },
        { label: "ID_DISTRITO_FED", key: "id_distrito_federal" },
        { label: "CABECERA_DISTRITAL_FED", key: "cabecera_distrital_federal" },
        { label: "ID_DISTRITO_LOC", key: "id_distrito_local" },
        { label: "CABECERA_DISTRITAL_LOC", key: "cabecera_distrital_local" },
        { label: "ID_MUNICIPIO_", key: "id_municipio_local" },
        { label: "MUNICIPIO", key: "municipio_local" },
        { label: "SECCION", key: "seccion" },
        { label: "ID_CASILLA", key: "id_casilla" },
        { label: "TIPO_CASILLA", key: "tipo_casilla" },
        { label: "EXT_CONTIGUA", key: "ext_contigua" },
        { label: "FECHA_HORA_RECEPCION", key: "fecha_hora_recepcion" },
        { label: "ESTADO_DEL_PAQUETE", key: "estado_del_paquete" },
        { label: "CARGO_RESPONSABLE_ENTREGA", key: "cargo_responsable_entrega" },
        { label: "PAQUETE_SELLADO", key: "paquete_sellado" },
        { label: "PAQ_SELLADO_ETIQUETA_SEG", key: "paq_sellado_etiqueta_seg" },
        { label: "ESTATUS_PAQUETE", key: "estatus_paquete" }
    ];

    //Metodo para obtener datos de la tabla y generar archivo csv
    const getDataByIdUsuario = () => {
        const data = {id_usuario:usuario.id}
        fetch(url + "/recepcion_paquetes_byidusuario", {
                method: "POST",
                headers:{
                    "Content-Type": "application/json"
                },
                body: JSON.stringify(data)
            })
            .then(response => response.json())
            .then(result => {
                setDataList(result)
                console.log(result)
                
            })
            .catch(error => {
                console.log(error)
            })
    }

    //Metodo get simple
    const getData = () => {
        fetch(url + "/recepcion_paquetes", {
            method: "GET",
            headers:{
                "Content-Type": "application/json"
            }
        })
            .then(response => response.json())
            .then(result => {

                setDataList(result)
                console.log(result)
                
            })
            .catch(error => {
                console.log(error)
            })
    }


    //Variables del documento
    const [fechaHoraRecepcion, setFechaHoraRecepcion] = useState()
    const [estadoDelPaquete, setEstadoDelPaquete] = useState("1")
    const [cargoResponsableEntrega, setCargoResponsableEntrega] = useState("P")
    const [paqueteSellado, setPaqueteSellado] = useState("S")
    const [paqSelladoEtiquetaSeg, setPaqSelladoEtiquetaSeg] = useState("S")
    const [estatusPaquete, setEstatusPaquete] = useState("1")

    //Metodo para nuevo registro de recepcion de paquetes
    const registrarData = (e) => {
        e.preventDefault()

        const data = {
            id_usuario: usuario.id,
            casilla_id: selectedOption.value.id,
            fecha_hora_recepcion: fechaHoraRecepcion,
            estado_del_paquete: estadoDelPaquete,
            cargo_responsable_entrega: cargoResponsableEntrega,
            paquete_sellado: paqueteSellado,
            paq_sellado_etiqueta_seg: paqSelladoEtiquetaSeg,
            estatus_paquete: estatusPaquete
        }

        fetch(url + "/registrar_recepcion_paquetes", {
                method: "POST",
                headers:{
                    "Content-Type": "application/json"
                },
                body: JSON.stringify(data)
            })
            .then(response => response.json())
            .then(result => {
                setMensaje("Registro guardado con éxito")
                handleShow()
                console.log(result)
                
            })
            .catch(error => {
                setMensaje(error)
                handleShow()
                console.log(error)
            })
    }

    useEffect(() => {
        if(usuario.rol === "Admin"){
            console.log(usuario.rol)
            getData()
            getCasillas()
        } else if(usuario.rol === "usuario"){
            console.log(usuario.rol)
            getDataByIdUsuario()
            getCasillas()
        }
        
    }, [])

    //variables para casilla
    const [options, setOptions] = useState([]);
    const [selectedOption, setSelectedOption] = useState(null);
    const [loading, setLoading] = useState(true);

    //metodo para obtener casillas
    const getCasillas = () => {
        fetch(url + "/get_casillas", {
            method: "GET",
            headers:{
                "Content-Type": "application/json"
            }
        })
            .then(response => response.json())
            .then(result => {
                const formattedOptions = result.map((item, index) => ({
                    value: item,
                    label: item.nombre_casilla
                }));
                setOptions(formattedOptions)
                setLoading(false)
                console.log(result)
                
            })
            .catch(error => {
                console.log(error)
            })
    }

    const SelectWithSearch = () => {
        const handleSelectChange = (selectedOption) => {
          setSelectedOption(selectedOption);
          console.log(selectedOption)
        };
      
        return (
          <div>
            {loading ? (
              <p>Cargando...</p>
            ) : (
              <Select
                value={selectedOption}
                onChange={handleSelectChange}
                options={options}
                placeholder="Buscar..."
              />
            )}
          </div>
        );
    };


    return(
        <>
        <Container>
            <h5>Recepción de paquetes</h5>
            <Row>
                <Col sm={10}>
                <p>Ingrese los siguientes datos para registrar la información de la fecha y hora de recepción del paquete electoral, así como el estado en el que éste se recibe</p>
                </Col>
                <Col sm={2}>
                <CSVLink data={dataList} headers={headers} filename="RECEPCION DE PAQUETES">
                        <Button className="button-custom">
                            {loading ? 'Generando...' : 'Descargar'}
                        </Button>
                    </CSVLink>
                </Col>
            </Row>



            <Form onSubmit={registrarData}>
            <Row className="justify-content-md-center mb-3">
                <Col xs lg="8">
                    <Card className="mt-1">
                        <Card.Body>
                            <Card.Text>
                                <Form.Group className="mb-4">
                                    <Form.Label>Casilla</Form.Label>
                                    <SelectWithSearch />
                                </Form.Group>
                                <Form.Group className="mb-4">
                                    <Form.Label>Registra la fecha y hora de recepción del paquete</Form.Label>
                                    <Form.Control type="datetime-local" required onChange={(e)=>setFechaHoraRecepcion(e.target.value)}/>
                                </Form.Group>
                                <Form.Group className="mb-4">
                                    <Form.Label>Estado del paquete electoral</Form.Label>
                                    <Form.Select onChange={(e)=>setEstadoDelPaquete(e.target.value)}>
                                        <option value="1">SIN MUESTRA DE ALTERACION Y FIRMADO</option>
                                        <option value="2">SIN MUESTRAS DE ALTERACION Y SIN FIRMAS</option>
                                        <option value="3">CON MUESTRAS DE ALTERACION Y FIRMADO</option>
                                        <option value="4">CON MUESTRAS DE ALTERACION Y SIN FIRMAS</option>
                                    </Form.Select>
                                </Form.Group>
                                <Form.Group className="mb-4">
                                    <Form.Label>Cargo del responsable de la entrega</Form.Label>
                                    <Form.Select onChange={(e)=>setCargoResponsableEntrega(e.target.value)}>
                                        <option value="P">SUPERVISOR ELECTORAL</option>
                                        <option value="P">CAPACITADOR ELECTORAL LOCAL</option>
                                        <option value="P">PRESIDENTE</option>
                                        <option value="S">SECRETARIO</option>
                                        <option value="1">PRIMER ESCRUTADOR</option>
                                        <option value="2">SEGUNDO ESCRUTADOR</option>
                                    </Form.Select>
                                </Form.Group>
                                <Form.Group className="mb-4">
                                    <Form.Label>Identifica si el paquete venía sellado</Form.Label>
                                    <Form.Select onChange={(e)=>setPaqueteSellado(e.target.value)}>
                                        <option value="S">SI</option>
                                        <option value="N">NO</option>
                                    </Form.Select>
                                </Form.Group>
                                <Form.Group className="mb-4">
                                    <Form.Label>Identifica si el paquete venía con etiquetas de seguridad</Form.Label>
                                    <Form.Select onChange={(e)=>setPaqSelladoEtiquetaSeg(e.target.value)}>
                                        <option value="S">SI</option>
                                        <option value="N">NO</option>
                                    </Form.Select>
                                </Form.Group>
                                <Form.Group className="mb-4">
                                    <Form.Label>Estatus de la recepción del paquete</Form.Label>
                                    <Form.Select onChange={(e)=>setEstatusPaquete(e.target.value)}>
                                        <option value="1">CASILLA NO INSTALADA</option>
                                        <option value="2">PAQUETE RECIBIDO</option>
                                        <option value="3">PAQUETE NO RECIBIDO</option>
                                        <option value="4">PAQUETE RECIBIDO DE FORMA EXTEMPORÁNEA CON CAUSA JUSTIFICADA</option>
                                        <option value="5">PAQUETE RECIBIDO DE FORMA EXTEMPORÁNEA SIN CAUSA JUSTIFICADA</option>
                                    </Form.Select>
                                </Form.Group>
                            </Card.Text>
                        </Card.Body>
                    </Card>
                    <div className="d-grid gap-2 mt-3">
                        <Button type="submit" className="button-custom">Guardar</Button>
                    </div>
                </Col>
            </Row>
            </Form>
        </Container>
        <Modal
            show={show}
            onHide={handleClose}
            backdrop="static"
            keyboard={false}
        >
            <Modal.Header closeButton>
                <Modal.Title></Modal.Title>
            </Modal.Header>
            <Modal.Body>
            {mensaje}
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={handleClose}>
                    Cerrar
                </Button>
            </Modal.Footer>
        </Modal>
        </>
    )

}

export default RecepcionPaquetes