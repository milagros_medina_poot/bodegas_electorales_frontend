import React, { useEffect, useState } from "react"
import { Container } from "react-bootstrap";
import {Row, Card, Button} from "react-bootstrap";
import {Col, Form} from "react-bootstrap";
import { CSVLink} from "react-csv";
import { Modal } from "react-bootstrap";
import {config} from "../Constants"

//prueba para subir a la rama desarrollo
const AperturaCierre = () => {

    const url = config.url.BASE_URL;

    let usuario = {}
    if(localStorage.getItem("usuario")){
        usuario = JSON.parse(localStorage.getItem("usuario"))
    }
    
    useEffect(() => {
        if(usuario.rol === "Admin"){
            console.log(usuario.rol)
            getData()
        } else if(usuario.rol === "usuario"){
            console.log(usuario.rol)
            getDataByIdUsuario()
        }
        
    }, [])

    

    const [mensaje, setMensaje] = useState("")
    const [show, setShow] = useState(false);
    const handleClose = () => {
        setShow(false);
        window.location.reload()
    }
    const handleShow = () => setShow(true);

    //Codigo para la generacion del archivo csv
    const [dataList, setDataList] = useState([])
    const headers = [
        { label: "ID_ESTADO", key: "id_estado" },
        { label: "NOMBRE_ESTADO", key: "nombre_estado" },
        { label: "ID_DISTRITO_FEDERAL", key: "id_distrito_federal" },
        { label: "CABECERA_DISTRITAL_FEDERAL", key: "cabecera_distrital_federal" },
        { label: "ID_DISTRITO_LOCAL", key: "id_distrito_local" },
        { label: "CABECERA_DISTRITAL_LOCAL", key: "cabecera_distrital_local" },
        { label: "ID_MUNICIPIO_LOCAL", key: "id_municipio_local" },
        { label: "MUNICIPIO_LOCAL", key: "municipio_local" },
        { label: "ID_APERTURA", key: "id_apertura" },
        { label: "FECHA_HORA_APERTURA", key: "apertura" },
        { label: "FECHA_HORA_CIERRE", key: "cierre" },
        { label: "CANTIDAD_MEDIOS", key: "cantidad_medios" },
        { label: "CANTIDAD_TECNICOS", key: "cantidad_tecnicos" },
        { label: "CANTIDAD_TE", key: "cantidad_te" },
        { label: "MOTIVO", key: "motivo" },
        { label: "CON_SELLO_PUERTA", key: "con_sello_puerta" },
        { label: "CON_SELLO_VENTANA", key: "con_sello_ventana" },
        { label: "OBSERVACIONES", key: "observaciones" },
        { label: "APLICO_SELLO_PUERTA", key: "aplico_sello_puerta" },
        { label: "APLICO_SELLO_VENTANA", key: "aplico_sello_ventana" },
        { label: "CANTIDAD_MEDIOS_CIERRE", key: "cantidad_medios_cierre" },
        { label: "CANTIDAD_TECNICOS_CIERRE", key: "cantidad_tecnicos_cierre" },
        { label: "CANTIDAD_TE_CIERRE", key: "cantidad_te_cierre" }
      ];

    //Metodo para obtener datos de la tabla y generar archivo csv
    const getDataByIdUsuario = () => {
        const data = {id_usuario:usuario.id}
        fetch(url + "/apertura_cierre_byidusuario", {
                method: "POST",
                headers:{
                    "Content-Type": "application/json"
                },
                body: JSON.stringify(data)
            })
            .then(response => response.json())
            .then(result => {
                setDataList(result)
                console.log(result)
                
            })
            .catch(error => {
                console.log(error)
            })
    }
    
    
    //Metodo get simple
    const getData = () => {
        fetch(url + "/apertura_cierre", {
            method: "GET",
            headers:{
                "Content-Type": "application/json"
            }
        })
            .then(response => response.json())
            .then(result => {

                setDataList(result)
                console.log(result)
                
            })
            .catch(error => {
                console.log(error)
            })
    }

    //variables del documento
    const [fecha_hora_apertura, setFechaHoraApertura] = useState("")
    const [fecha_hora_cierre, setFechaHoraCierre] = useState("")
    const [cantidad_medios, setCantidadMedios] = useState("")
    const [cantidad_tecnicos, setCantidadTecnicos] = useState("")
    const [cantidad_te, setCantidadTe] = useState("")
    const [motivo, setMotivo] = useState("CONTEO DE BOLETAS")
    const [con_sello_puerta, setConSelloPuerta] = useState("S")
    const [con_sello_ventana, setConSelloVentana] = useState("A")
    const [observaciones, setObservaciones] = useState("NINGUNA")
    const [aplico_sello_puerta, setAplicoSelloPuerta] = useState("S")
    const [aplico_sello_ventana, setAplicoSelloVentana] = useState("A")
    const [cantidad_medios_cierre, setCantidadMediosCierre] = useState("")
    const [cantidad_tecnicos_cierre, setCantidadTecnicosCierre] = useState("")
    const [cantidad_te_cierre, setCantidadTeCierre] = useState("")

    //Validacion de strings
    const [errorObs, setErrorObs] = useState(false)

    const handleObs = (e) => {
        const regex = RegExp(/^[A-Za-z ]+$/)
        setObservaciones(e.target.value)

        if(e.target.value != ""){
            if(!regex.test(e.target.value)){
                setErrorObs(true)
            }else{
                setErrorObs(false)
            }
        }else{
            setObservaciones("NINGUNA")
            setErrorObs(false)
        }
        
    }

    //Metodo para nuevo registro de apertura y cierre de bodega
    const registrarAperturaCierre = (e) => {
        e.preventDefault()
        const data = {
            id_usuario: usuario.id,
            id_apertura: 1,
            fecha_hora_apertura: fecha_hora_apertura,
            fecha_hora_cierre: fecha_hora_cierre,
            cantidad_medios: cantidad_medios,
            cantidad_tecnicos: cantidad_tecnicos,
            cantidad_te: cantidad_te,
            motivo: motivo,
            con_sello_puerta: con_sello_puerta,
            con_sello_ventana: con_sello_ventana,
            observaciones: observaciones.toLocaleUpperCase(),
            aplico_sello_puerta: aplico_sello_puerta,
            aplico_sello_ventana: aplico_sello_ventana,
            cantidad_medios_cierre: cantidad_medios_cierre,
            cantidad_tecnicos_cierre: cantidad_tecnicos_cierre,
            cantidad_te_cierre: cantidad_te_cierre
        }

        if(!errorObs){
            fetch(url + "/registrar_apertura_cierre", {
                method: "POST",
                headers:{
                    "Content-Type": "application/json"
                },
                body: JSON.stringify(data)
            })
            .then(response => response.json())
            .then(result => {
                setMensaje("Registro guardado con éxito")
                handleShow()
                console.log(result)
                
            })
            .catch(error => {
                setMensaje(error)
                handleShow()
                console.log(error)
            })
        }
    }

    const [loading, setLoading] = useState(false);

    const fetchData = async () => {
        setLoading(true);
        try {
            fetch(url + "/apertura_cierre", {
                method: "GET",
                headers:{
                    "Content-Type": "application/json"
                }
            })
                .then(response => response.json())
                .then(result => {
    
                    setDataList(result)
                    console.log(result)
                    
                })
                .catch(error => {
                    console.log(error)
                })
        } catch (error) {
          console.error('Error fetching data', error);
        }
        setLoading(false);
    }

    const handleDownload = async () => {
        await fetchData();
    };

    

    return(
        <>
        <Container>
            <h5>Apertura y cierre de bodegas electorales</h5>
            <Row>
                <Col sm={10}>
                    <p>Ingrese los siguientes datos para el registro de la bitácora de apertura y cierre de bodegas</p>
                </Col>
                <Col sm={2}>
                    <CSVLink data={dataList} headers={headers} filename="APERTURA Y CIERRE DE BODEGAS">
                        <Button className="button-custom">
                            {loading ? 'Generando...' : 'Descargar'}
                        </Button>
                    </CSVLink>
                </Col>
            </Row>
            
            
            <Form onSubmit={registrarAperturaCierre}>
            <Row className="mb-3">
                <Col xs lg="6">
                    <Card className="mt-1">
                        <Card.Body>
                            <Card.Text>
                                <Form.Group className="mb-4">
                                    <Form.Label>Registra la fecha y hora en que se abrió la bodega</Form.Label>
                                    <Form.Control type="datetime-local" required onChange={(e)=>setFechaHoraApertura(e.target.value)}/>
                                </Form.Group>
                                <Form.Group className="mb-4">
                                    <Form.Label>Registra la fecha y hora en que se cerró la bodega</Form.Label>
                                    <Form.Control type="datetime-local" required onChange={(e)=>setFechaHoraCierre(e.target.value)}/>
                                </Form.Group>
                                <Form.Group className="mb-4">
                                    <Form.Label>Cantidad de medios de comunicación que estuvieron presentes en la apertura</Form.Label>
                                    <Form.Control type="number" min={0} required onChange={(e)=>setCantidadMedios(e.target.value)}/>
                                </Form.Group>
                                <Form.Group className="mb-4">
                                    <Form.Label>Cantidad de personal administrativo que estuvo presente en la apertura</Form.Label>
                                    <Form.Control type="number" min={0} required onChange={(e)=>setCantidadTecnicos(e.target.value)}/>
                                </Form.Group>
                                <Form.Group className="mb-4">
                                    <Form.Label>Cantidad de personas del Tribunal Electoral que estuvieron presentes en la apertura</Form.Label>
                                    <Form.Control type="number" min={0} required onChange={(e)=>setCantidadTe(e.target.value)}/>
                                </Form.Group>
                                <Form.Group className="mb-4">
                                    <Form.Label>Motivo de la apertura de la bodega</Form.Label>
                                    <Form.Select onChange={(e)=>setMotivo(e.target.value)}>
                                        <option value="CONTEO DE BOLETAS">Conteo de Boletas</option>
                                        <option value="SELLADO DE BOLETAS">Selleado de Boletas</option>
                                        <option value="INTEGRACION DE CAJAS PAQUETES Y MATERIAL ELECTORAL">Integracion de Cajas Paquetes y Material Electoral</option>
                                        <option value="ENTREGA DE PAQUETES A FMDC">Entrega de Paquetes a FMDC</option>
                                        <option value="JORNADA ELECTORAL">Jornada Electoral</option>
                                        <option value="COMPUTOS DISTRITALES">Computos Distritales</option>
                                        <option value="COMPUTOS MUNICIPALES">Computos Municipales</option>
                                    </Form.Select>
                                </Form.Group>
                                <Form.Group className="mb-4">
                                    <Form.Label>Observaciones en la apertura de la bodega</Form.Label>
                                    <Form.Control as="textarea" onChange={(e)=>handleObs(e)} style={{ textTransform: 'uppercase'}}/>
                                    <Form.Text className="text-muted" id="error">
                                        Solo se permiten letras. Sin acentos ni caracteres numéricos o especiales.
                                    </Form.Text>
                                    {errorObs && <p style={{color: "red"}}>El texto es inválido</p>}
                                </Form.Group>
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Col>

                <Col xs lg="6">
                    <Card className="mt-1">
                        <Card.Body>
                            <Card.Text>
                                <Form.Group className="mb-4">
                                    <Form.Label>En la apertura de la bodega, ¿ésta contaba con sello en la(s) puerta(s)?</Form.Label>
                                    <Form.Select onChange={(e)=>setConSelloPuerta(e.target.value)}>
                                        <option value="S">SI</option>
                                        <option value="N">NO</option>
                                    </Form.Select>
                                </Form.Group>
                                <Form.Group className="mb-4">
                                    <Form.Label>En la apertura de la bodega, ¿ésta contaba con sello en la(s) ventana(s)?</Form.Label>
                                    <Form.Select onChange={(e)=>setConSelloVentana(e.target.value)}>
                                        <option value="A">NO APLICA</option>
                                        <option value="S">SI</option>
                                        <option value="N">NO</option>
                                    </Form.Select>
                                </Form.Group>
                                <Form.Group className="mb-4">
                                    <Form.Label>En el cierre de la bodega, ¿se aplicó sello en la(s) puerta(s)?</Form.Label>
                                    <Form.Select onChange={(e)=>setAplicoSelloPuerta(e.target.value)}>
                                        <option value="S">SI</option>
                                        <option value="N">NO</option>
                                    </Form.Select>
                                </Form.Group>
                                <Form.Group className="mb-4">
                                    <Form.Label>En el cierre de la bodega, ¿se aplicó sello en la(s) ventana(s)?</Form.Label>
                                    <Form.Select onChange={(e)=>setAplicoSelloVentana(e.target.value)}>
                                        <option value="A">NO APLICA</option>
                                        <option value="S">SI</option>
                                        <option value="N">NO</option>
                                    </Form.Select>
                                </Form.Group>
                                <Form.Group className="mb-4">
                                    <Form.Label>Cantidad de medios de comunicación que estuvieron presentes en el cierre</Form.Label>
                                    <Form.Control type="number" min={0} required onChange={(e)=>setCantidadMediosCierre(e.target.value)}/>
                                </Form.Group>
                                <Form.Group className="mb-4">
                                    <Form.Label>Cantidad de personal administrativo que estuvo presente en el cierre</Form.Label>
                                    <Form.Control type="number" min={0} required onChange={(e)=>setCantidadTecnicosCierre(e.target.value)}/>
                                </Form.Group>
                                <Form.Group className="mb-4">
                                    <Form.Label>Cantidad de personas del Tribunal Electoral que estuvieron presentes en el cierre</Form.Label>
                                    <Form.Control type="number" min={0} required onChange={(e)=>setCantidadTeCierre(e.target.value)}/>
                                </Form.Group>
                            </Card.Text>
                        </Card.Body>
                    </Card>
                    <div className="d-grid gap-2 mt-3">
                        <Button type="submit" className="button-custom">Guardar</Button>
                    </div>
                </Col>
            </Row>
            </Form>
        </Container>

        <Modal
            show={show}
            onHide={handleClose}
            backdrop="static"
            keyboard={false}
        >
            <Modal.Header closeButton>
                <Modal.Title></Modal.Title>
            </Modal.Header>
            <Modal.Body>
            {mensaje}
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={handleClose}>
                    Cerrar
                </Button>
            </Modal.Footer>
        </Modal>
        </>
    )

}

export default AperturaCierre