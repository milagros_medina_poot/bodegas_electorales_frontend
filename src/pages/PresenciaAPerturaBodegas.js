import React, { useEffect, useState } from "react"
import { Container } from "react-bootstrap";
import {Row, Card, Button} from "react-bootstrap";
import {Col, Form} from "react-bootstrap";
import { CSVLink } from "react-csv";
import { Modal } from "react-bootstrap";
import {config} from "../Constants"
const url = config.url.BASE_URL;

const PresenciaAperturaBodegas = () => {
    
    let usuario = {}
    if(localStorage.getItem("usuario")){
        usuario = JSON.parse(localStorage.getItem("usuario"))
    }

    const [mensaje, setMensaje] = useState("")
    const [show, setShow] = useState(false);
    const handleClose = () => {
        setShow(false);
        window.location.reload()
    }
    const handleShow = () => setShow(true);

    //Código para la generacion del archivo csv
    const [dataList, setDataList] = useState([])
    const headers = [
        { label: "ID_ESTADO", key: "id_estado" },
        { label: "NOMBRE_ESTADO", key: "nombre_estado" },
        { label: "ID_DISTRITO_FED", key: "id_distrito_federal" },
        { label: "CABECERA_DISTRITAL_FED", key: "cabecera_distrital_federal" },
        { label: "ID_DISTRITO_LOC", key: "id_distrito_local" },
        { label: "CABECERA_DISTRITAL_LOC", key: "cabecera_distrital_local" },
        { label: "ID_MUNICIPIO", key: "id_municipio_local" },
        { label: "MUNICIPIO", key: "municipio_local" },
        { label: "ID_PRESENCIA_APERTURA", key: "id_presencia_apertura" },
        { label: "ID_APERTURA", key: "id_apertura" },
        { label: "TIPO_PUESTO", key: "tipo_puesto" },
        { label: "PRESENCIA_APERT_BODEGAS", key: "presencia_apert_bodegas" },
        { label: "CIERRE_APERTURA", key: "cierre_apertura" }
    ];

    //Metodo para obtener datos de la tabla y generar archivo csv
    const getDataByIdUsuario = () => {
        const data = {id_usuario:usuario.id}
        fetch(url + "/presencia_apertura_bodegas_byidusuario", {
                method: "POST",
                headers:{
                    "Content-Type": "application/json"
                },
                body: JSON.stringify(data)
            })
            .then(response => response.json())
            .then(result => {
                setDataList(result)
                console.log(result)
                
            })
            .catch(error => {
                console.log(error)
            })
    }

    //Metodo get simple
    const getData = () => {
        fetch(url + "/presencia_apertura_bodegas", {
            method: "GET",
            headers:{
                "Content-Type": "application/json"
            }
        })
            .then(response => response.json())
            .then(result => {

                setDataList(result)
                console.log(result)
                
            })
            .catch(error => {
                console.log(error)
            })
    }

    //checkboxes
    const [checkboxes, setCheckboxes] = useState({
        opcion1: false,
        opcion2: false,
        opcion3: false,
        opcion4: false,
        opcion5: false,
        opcion6: false,
        opcion7: false,
        opcion8: false,
        opcion9: false,
        opcion10: false,
        opcion11: false,
        opcion12: false
    });

    const handleChange = (event) => {
        const { name, checked } = event.target;
        setCheckboxes({
          ...checkboxes,
          [name]: checked,
        });
    };

    const registrarData = (e) => {
        e.preventDefault()
        const records = [
            {
                id_usuario: usuario.id,
                id_presencia_apertura: 1,
                id_apertura: 1,
                tipo_puesto: "CONSEJERIA DE PRESIDENCIA",
                presencia_apert_bodegas: checkboxes.opcion1 ? "S" : "N",
                cierre_apertura: checkboxes.opcion7 ? "C" : "A"
            },
            {
                id_usuario: usuario.id,
                id_presencia_apertura: 1,
                id_apertura: 1,
                tipo_puesto: "CONSEJERIAS ELECTORALES",
                presencia_apert_bodegas: checkboxes.opcion2 ? "S" : "N",
                cierre_apertura: checkboxes.opcion8 ? "C" : "A"
            },
            {
                id_usuario: usuario.id,
                id_presencia_apertura: 1,
                id_apertura: 1,
                tipo_puesto: "VOCALIA SECRETARIAL",
                presencia_apert_bodegas: checkboxes.opcion3 ? "S" : "N",
                cierre_apertura: checkboxes.opcion9 ? "C" : "A"
            },
            {
                id_usuario: usuario.id,
                id_presencia_apertura: 1,
                id_apertura: 1,
                tipo_puesto: "VOCALIA DE ORGANIZACION",
                presencia_apert_bodegas: checkboxes.opcion4 ? "S" : "N",
                cierre_apertura: checkboxes.opcion10 ? "C" : "A"
            },
            {
                id_usuario: usuario.id,
                id_presencia_apertura: 1,
                id_apertura: 1,
                tipo_puesto: "VOCALIA DE CAPACITACION",
                presencia_apert_bodegas: checkboxes.opcion5 ? "S" : "N",
                cierre_apertura: checkboxes.opcion11 ? "C" : "A"
            },
            {
                id_usuario: usuario.id,
                id_presencia_apertura: 1,
                id_apertura: 1,
                tipo_puesto: "REPRESENTACIONES",
                presencia_apert_bodegas: checkboxes.opcion6 ? "S" : "N",
                cierre_apertura: checkboxes.opcion12 ? "C" : "A"
            }
        ]

        fetch(url + "/registrar_varios_presencia_apertura_bodegas", {
                method: "POST",
                headers:{
                    "Content-Type": "application/json"
                },
                body: JSON.stringify({records})
            })
            .then(response => response.json())
            .then(result => {
                setMensaje(result.message)
                handleShow()
                console.log(result)
            })
            .catch(error => {
                setMensaje(error)
                handleShow()
                console.log(error)
            })
    }

    useEffect(() => {
        if(usuario.rol === "Admin"){
            console.log(usuario.rol)
            getData()
        } else if(usuario.rol === "usuario"){
            console.log(usuario.rol)
            getDataByIdUsuario()
        }
        
    }, [])

    const [loading, setLoading] = useState(false);

    return(
        <>
        <Container>
            <h5>Presencia de apertura de bodegas</h5>
            <Row>
                <Col sm={10}>
                <p>Ingrese los siguientes datos para el registro de la información de los funcionarios que asistieron a la apertura y/o cierre de la bodega</p>
                </Col>
                <Col sm={2}>
                <CSVLink data={dataList} headers={headers} filename="PRESENCIA DE APERTURA DE BODEGAS">
                        <Button className="button-custom">
                            {loading ? 'Generando...' : 'Descargar'}
                        </Button>
                    </CSVLink>
                </Col>
            </Row>


            <Form onSubmit={registrarData}>
            <Row className="justify-content-md-center mb-3">
                <Col xs lg="4">
                    <Card className="mt-1">
                        <Card.Body>
                            <Card.Text>
                                <Form.Group className="mb-4">
                                    <Form.Label>Presencia en la Apertura de bodega</Form.Label>
                                    <Form.Check
                                        label="Consejería de presidencia"
                                        name="opcion1"
                                        type="checkbox"
                                        checked={checkboxes.opcion1}
                                        onChange={handleChange}
                                        className="mt-2"
                                    />
                                    <Form.Check
                                        label="Consejerías electorales"
                                        name="opcion2"
                                        type="checkbox"
                                        checked={checkboxes.opcion2}
                                        onChange={handleChange}
                                        className="mt-2"
                                    />
                                    <Form.Check
                                        label="Vocalía secretarial"
                                        name="opcion3"
                                        type="checkbox"
                                        checked={checkboxes.opcion3}
                                        onChange={handleChange}
                                        className="mt-2"
                                    />
                                    <Form.Check
                                        label="Vocalía de organización"
                                        name="opcion4"
                                        type="checkbox"
                                        checked={checkboxes.opcion4}
                                        onChange={handleChange}
                                        className="mt-2"
                                    />
                                    <Form.Check
                                        label="Vocalía de capacitación"
                                        name="opcion5"
                                        type="checkbox"
                                        checked={checkboxes.opcion5}
                                        onChange={handleChange}
                                        className="mt-2"
                                    />
                                    <Form.Check
                                        label="Representaciones partidistas"
                                        name="opcion6"
                                        type="checkbox"
                                        checked={checkboxes.opcion6}
                                        onChange={handleChange}
                                        className="mt-2"
                                    />
                                </Form.Group>
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Col>
                <Col xs lg="4">
                    <Card className="mt-1">
                        <Card.Body>
                            <Card.Text>
                                <Form.Group className="mb-4">
                                    <Form.Label>Presencia en el Cierre de bodega</Form.Label>
                                    <Form.Check
                                        label="Consejería de presidencia"
                                        name="opcion7"
                                        type="checkbox"
                                        checked={checkboxes.opcion7}
                                        onChange={handleChange}
                                        className="mt-2"
                                    />
                                    <Form.Check
                                        label="Consejerías electorales"
                                        name="opcion8"
                                        type="checkbox"
                                        checked={checkboxes.opcion8}
                                        onChange={handleChange}
                                        className="mt-2"
                                    />
                                    <Form.Check
                                        label="Vocalía secretarial"
                                        name="opcion9"
                                        type="checkbox"
                                        checked={checkboxes.opcion9}
                                        onChange={handleChange}
                                        className="mt-2"
                                    />
                                    <Form.Check
                                        label="Vocalía de organización"
                                        name="opcion10"
                                        type="checkbox"
                                        checked={checkboxes.opcion10}
                                        onChange={handleChange}
                                        className="mt-2"
                                    />
                                    <Form.Check
                                        label="Vocalía de capacitación"
                                        name="opcion11"
                                        type="checkbox"
                                        checked={checkboxes.opcion11}
                                        onChange={handleChange}
                                        className="mt-2"
                                    />
                                    <Form.Check
                                        label="Representaciones partidistas"
                                        name="opcion12"
                                        type="checkbox"
                                        checked={checkboxes.opcion12}
                                        onChange={handleChange}
                                        className="mt-2"
                                    />
                                </Form.Group>
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Col>
                <Row>
                    <Col md={{ span: 8, offset: 2 }}>
                        <div className="d-grid mt-3">
                            <Button type="submit" className="button-custom">Guardar</Button>
                        </div>
                    </Col>
                </Row>
            </Row>
            </Form>
        </Container>
        <Modal
            show={show}
            onHide={handleClose}
            backdrop="static"
            keyboard={false}
        >
            <Modal.Header closeButton>
                <Modal.Title></Modal.Title>
            </Modal.Header>
            <Modal.Body>
            {mensaje}
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={handleClose}>
                    Cerrar
                </Button>
            </Modal.Footer>
        </Modal>
        </>
    )

}

export default PresenciaAperturaBodegas