import React, { useEffect, useState } from "react"
import { Container } from "react-bootstrap";
import {Row, Card, Button} from "react-bootstrap";
import {Col, Form} from "react-bootstrap";
import { CSVLink, CSVDownload } from "react-csv";
import { Modal } from "react-bootstrap";
import {config} from "../Constants"

const DetallePresenciaAperturaBodega = () => {
    const url = config.url.BASE_URL;
    let usuario = {}
    if(localStorage.getItem("usuario")){
        usuario = JSON.parse(localStorage.getItem("usuario"))
    }

    const [mensaje, setMensaje] = useState("")
    const [show, setShow] = useState(false);
    const handleClose = () => {
        setShow(false);
        window.location.reload()
    }
    const handleShow = () => setShow(true);

    //Código para la generacion del archivo csv
    const [dataList, setDataList] = useState([])
    const headers = [
        { label: "ID_ESTADO", key: "id_estado" },
        { label: "NOMBRE_ESTADO", key: "nombre_estado" },
        { label: "ID_DISTRITO_FEDERAL", key: "id_distrito_federal" },
        { label: "CABECERA_DISTRITAL_FEDERAL", key: "cabecera_distrital_federal" },
        { label: "ID_DISTRITO_LOCAL", key: "id_distrito_local" },
        { label: "CABECERA_DISTRITAL_LOCAL", key: "cabecera_distrital_local" },
        { label: "ID_MUNICIPIO_LOCAL", key: "id_municipio_local" },
        { label: "MUNICIPIO_LOCAL", key: "municipio_local" },
        { label: "ID_APERTURA", key: "id_apertura" },
        { label: "ID_DETALLE_PRESENCIA", key: "id_detalle_presencia" },
        { label: "ID_PARTIDO_POLITICO", key: "id_partido_politico" },
        { label: "PARTIDO_POLITICO", key: "partido_politico" },
        { label: "TIPO_PUESTO", key: "tipo_puesto" },
        { label: "NOMBRE_REPRESENTANTE", key: "nombre_representante" }
    ];

    //Metodo para obtener datos de la tabla y generar archivo csv
    const getDataByIdUsuario = () => {
        const data = {id_usuario:usuario.id}
        fetch(url + "/detalle_presencia_apertura_bodega_byidusuario", {
                method: "POST",
                headers:{
                    "Content-Type": "application/json"
                },
                body: JSON.stringify(data)
            })
            .then(response => response.json())
            .then(result => {
                setDataList(result)
                console.log(result)
                
            })
            .catch(error => {
                console.log(error)
            })
    }

    //Metodo get simple
    const getData = () => {
        fetch(url + "/detalle_presencia_apertura_bodega", {
            method: "GET",
            headers:{
                "Content-Type": "application/json"
            }
        })
            .then(response => response.json())
            .then(result => {

                setDataList(result)
                console.log(result)
                
            })
            .catch(error => {
                console.log(error)
            })
    }


    //Variables del documento
    const [idPartidoPolitico, setIdPartidoPolitico] = useState()
    const [partidoPolitico, setPartidoPolitico] = useState()
    const [tipoPuesto, setTipoPuesto] = useState("P")
    const [nombreRepresentante, setNombreRepresentante] = useState()

    //Validacion de campos
    const [error, setError] = useState(false)
    const regex = RegExp(/^[A-Za-z ]+$/)

    //validacion de campos
    const handleError = (e) => {
        setNombreRepresentante(e.target.value)
        if(!regex.test(e.target.value)){
            setError(true)
        }else{
            setError(false)
        }
    }

    //handle partido politico
    const handlePartidoPolitico = (e) => {
        const opcionSeleccionada = JSON.parse(e.target.value);
        setIdPartidoPolitico(opcionSeleccionada.id_partido_politico)
        setPartidoPolitico(opcionSeleccionada.partido_politico)
    }

    //Metodo para nuevo registro de detalle de presencia de apertura de bodega
    const registrarData = (e) => {
        e.preventDefault()

        const data = {
            id_usuario: usuario.id,
            id_apertura: 1,
            id_detalle_presencia: 1,
            id_partido_politico: idPartidoPolitico,
            partido_politico: partidoPolitico,
            tipo_puesto: tipoPuesto,
            nombre_representante: nombreRepresentante.toLocaleUpperCase(),
        }

        if(!error){
            console.log("form valido")
            fetch(url + "/registrar_detalle_presencia_apertura_bodega", {
                method: "POST",
                headers:{
                    "Content-Type": "application/json"
                },
                body: JSON.stringify(data)
            })
            .then(response => response.json())
            .then(result => {
                setMensaje("Registro guardado con éxito")
                handleShow()
                console.log(result)
                
            })
            .catch(error => {
                setMensaje(error)
                handleShow()
                console.log(error)
            })

        }
    }

    //obtener partidos politicos para select
    const [partidosPoliticos, setPartidosPoliticos] = useState([])

    // Generar opciones a partir del array de objetos de partidos politicos
    const opcionesSelectPartidosPoliticos = partidosPoliticos.map((opcion) => (
        <option value={JSON.stringify(opcion)}>
          {opcion.partido_politico}
        </option>
      ));

    const getPartidosPoliticos = () => {
        fetch(url + "/partidos_politicos", {
            method: "GET",
            headers:{
                "Content-Type": "application/json"
            }
        })
            .then(response => response.json())
            .then(result => {

                setPartidosPoliticos(result)
                setIdPartidoPolitico(result[0].id_partido_politico)
                setPartidoPolitico(result[0].partido_politico)
                console.log(result)
                
            })
            .catch(error => {
                console.log(error)
            })
    }

    useEffect(() => {
        if(usuario.rol === "Admin"){
            console.log(usuario.rol)
            getData()
            getPartidosPoliticos()
        } else if(usuario.rol === "usuario"){
            console.log(usuario.rol)
            getDataByIdUsuario()
            getPartidosPoliticos()
        }
        
    }, [])

    const [loading, setLoading] = useState(false);

    return(
        <>
        <Container>
            <h5>Detalle de presencia de apertura de bodega</h5>
            
            <Row>
                <Col sm={10}>
                <p>Ingrese los siguientes datos para el registro de la información de los representantes de partidos y candidatos independientes que asistieron a la apertura de bodega</p>
                </Col>
                <Col sm={2}>
                <CSVLink data={dataList} headers={headers} filename="DETALLE PRESENCIA DE APERTURA DE BODEGA">
                        <Button className="button-custom">
                            {loading ? 'Generando...' : 'Descargar'}
                        </Button>
                    </CSVLink>
                </Col>
            </Row>



            <Form onSubmit={registrarData}>
            <Row className="justify-content-md-center mb-3">
                <Col xs lg="8">
                    <Card className="mt-1">
                        <Card.Body>
                            <Card.Text>
                                <Form.Group className="mb-4">
                                    <Form.Label>Partido politico</Form.Label>
                                    <Form.Select onChange={(e)=>handlePartidoPolitico(e)}>
                                        { opcionesSelectPartidosPoliticos}
                                    </Form.Select>
                                </Form.Group>
                                <Form.Group className="mb-4">
                                    <Form.Label>Tipo de puesto</Form.Label>
                                    <Form.Select onChange={(e)=>setTipoPuesto(e.target.value)}>
                                        <option value="P">PROPIETARIO</option>
                                        <option value="S">SUPLENTE</option>
                                    </Form.Select>
                                </Form.Group>
                                <Form.Group className="mb-4">
                                    <Form.Label>Nombre del representante del partido politico que estuvo en la apertura de la bodega</Form.Label>
                                    <Form.Control required onChange={(e)=>handleError(e)} style={{ textTransform: 'uppercase'}}/>
                                    <Form.Text className="text-muted" id="error">
                                    Solo se permiten letras. Sin acentos ni caracteres numéricos o especiales.
                                    </Form.Text>
                                    {error && <p style={{color: "red"}}>El texto es inválido</p>}
                                </Form.Group>
                                
                            </Card.Text>
                        </Card.Body>
                    </Card>
                    <div className="d-grid gap-2 mt-3">
                        <Button type="submit" className="button-custom">Guardar</Button>
                    </div>
                </Col>
            </Row>
            </Form>
        </Container>
        <Modal
            show={show}
            onHide={handleClose}
            backdrop="static"
            keyboard={false}
        >
            <Modal.Header closeButton>
                <Modal.Title></Modal.Title>
            </Modal.Header>
            <Modal.Body>
            {mensaje}
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={handleClose}>
                    Cerrar
                </Button>
            </Modal.Footer>
        </Modal>
        </>
    )

}

export default DetallePresenciaAperturaBodega