import React, { useEffect, useState } from "react"
import Home from "./Home"
import { Container } from "react-bootstrap";
import {Row, Card, Button} from "react-bootstrap";
import {Col, Form} from "react-bootstrap";
import {config} from "../Constants"

function parseJwt (token) {
    const base64Url = token.split('.')[1];
    const base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
    const jsonPayload = decodeURIComponent(window.atob(base64).split('').map(function(c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));

    return JSON.parse(jsonPayload);
}

const Login = () => {

    const url = config.url.BASE_URL;

    const [tokenExistAndStillValid, setTokenExistAndStillValid] = useState(false)

    useEffect(() => {
        if(localStorage.getItem("token")){
            setTokenExistAndStillValid(parseJwt(localStorage.getItem('token')).exp * 1000 > Date.now())
        }
    }, []);

    const [usuario, setUsuario] = useState("")
    const [password, setPassword] = useState("")
    const [error, setError] = useState(false)

    const handleKeyDown = (event) => {
        if(event.keyCode === 13) {
            handleLogin(event)
      }
    }

    const handleLogin = (e) => {
        e.preventDefault()

        const data = {
            usuario: usuario,
            password: password
        }

        fetch(url + "/login", {
            method: "POST",
            headers:{
                "Content-Type": "application/json"
            },
            body: JSON.stringify(data)
        })
            .then(response => response.json())
            .then(result => {
                if(result.token){
                    localStorage.setItem("token", result.token)
                    localStorage.setItem("usuario", JSON.stringify(result))
                    window.location.reload()
                }else{
                    setError(true)
                }
                
            })
            .catch(error => {
                console.log(error)
            })
    }

    return(
        <>
            {
                tokenExistAndStillValid ? <Home/> : 
                <Container className="container mt-5">
                    <Row className="justify-content-md-center">
                    <h4>IEQROO | Sistema para Registro de Bitácoras</h4>
                        <Col xs lg="4">
                            <Card className="mt-5">
                                <Form onSubmit={handleLogin} onKeyDown={handleKeyDown}>
                                    <Card.Header>Inicio de sesión</Card.Header>
                                    <Card.Body>
                                        <Card.Text>
                                            <Form.Group className="mb-3" >
                                                <Form.Label>Usuario:</Form.Label>
                                                <Form.Control required type="text" placeholder="Usuario" onChange={(e)=>setUsuario(e.target.value)}/>
                                            </Form.Group>

                                            <Form.Group className="mb-3" controlId="formBasicPassword">
                                                <Form.Label>Contraseña:</Form.Label>
                                                <Form.Control required type="password" placeholder="Contraseña" onChange={(e)=>setPassword(e.target.value)}/>
                                            </Form.Group>
                                        </Card.Text>
                                        {error && <p style={{color: "red"}}>Usuario o contraseña inválido</p>}
                                    </Card.Body>
                                    <Card.Footer>
                                        <div className="d-grid gap-2 mt-3">
                                            <Button type="submit" className="button-custom">
                                                Iniciar sesión
                                            </Button>
                                        </div>
                                    </Card.Footer>
                                </Form>
                            </Card>
                        </Col>
                    </Row>
                </Container>
            }
        </>
        
    )
}

export default Login