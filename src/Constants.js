
const prod = {
	url: {
		BASE_URL: 'http://ec2-3-16-31-29.us-east-2.compute.amazonaws.com:5000',
		AUTH_URL: '',
	},
};

const dev = {
	url: {
		BASE_URL: 'http://localhost:4000',
		AUTH_URL: '',
	},
};

//export const config = process.env.NODE_ENV === 'development' ? dev : prod;
export const config = prod;
